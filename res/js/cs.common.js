$(function() {

    // DONT MODIFY BELOW, IT'LL CHANGE HOW THE DASHBOARD WORKS //
    var _wlh = $('.logo-holder').width();
    $('.sidebar-menu-holder').css("width", (_wlh+20) + "px");

    $(window).resize(function() {
        _wlh = $('.logo-holder').width();
        $('.sidebar-menu-holder').css("width", (_wlh+20) + "px");
    });

    $('li.menu-item').each(function() {
        if($(this).has('ul.sub-menu'))
            $(this).addClass('has-sub');
    });

    $('li.menu-item').has('ul.sub-menu').has('li').prepend("<span class='pull_right arrow'><i class='icon-right-open-mini'> </i></span>");
    $('li.menu-item').has('ul.sub-menu').has('li').click(function() {
        $('.has-sub').not(this).removeClass('active');
        $('.has-sub').not(this).find('ul.sub-menu').slideUp('fast');
        $('.has-sub').not(this).find('.arrow').removeClass('rotate');
        $(this).find('ul.sub-menu').slideToggle('fast');
        $(this).find('.arrow').toggleClass('rotate');
        $(this).toggleClass('active');
    });

    $('ul.sub-menu').click(function(e) {
        e.stopPropagation();
    });



    $(".mob-menu").click(function() {
        $(this).toggleClass('active');
        $('.sidebar-menu-holder').slideToggle("fast");
    });
});
