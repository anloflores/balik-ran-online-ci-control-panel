$(function() {

    Chart.defaults.global.legend.display = false;
    Chart.defaults.global.title.display = false;

    var ctx = document.getElementById("myChart");
    var ctx2 = document.getElementById("myChart2");
    var ctx3 = document.getElementById("myChart3");
    var ctx4 = document.getElementById("myChart4");

    var data = {
        labels: ["", "", "", "", "", "", ""],
        datasets: [
            {
                label: "My First dataset",
                fill: false,
                lineTension: 0.3,
                backgroundColor: "#fff",
                borderColor: "#fff",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "#fff",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "#fff",
                pointHoverBorderColor: "#fff",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: [65, 59, 80, 81, 56, 55, 40],
                spanGaps: false,
            }
        ]
    };

    var data3  = {
           labels: ["1", "2", "3", "4", "5"],
           datasets: [

               {
                 label: 'Mobile',
                 backgroundColor: 'rgba(193, 78, 49, .6)',
                 borderColor: 'rgba(193, 78, 49, 0.50)',
                 pointColor: 'rgba(193, 78, 49, 0.50)',
                 pointStrokeColor: 'rgba(193, 78, 49, 0.50)',
                 pointHighlightStroke: 'rgba(193, 78, 49, 0.50)',
                 data: [0,10,30,40,0]
             },
               {
                 label: 'Tablet',
                 backgroundColor: 'rgba(37,62,76,.6)',
                 borderColor: 'rgba(37,62,76,.50)',
                 pointColor: 'rgba(37,62,76,.50)',
                 pointStrokeColor: 'rgba(37,62,76,.50)',
                 pointHighlightStroke: 'rgba(37,62,76,.50)',
                 data: [0,20,40,50,0]
             },
               {
                 label: 'Desktop',
                 backgroundColor: '#7fdfff',
                 pointColor: '#7fdfff',
                 pointStrokeColor: '#7fdfff',
                 pointHighlightStroke: '#7fdfff',
                 data: [0,30,50,40,0]
             }
           ]
       };

       var myChart4 = new Chart(ctx4, {
           type: 'line',
           data: data3,
           options:
           {
               scales:
               {
                   xAxes: [{
                       display: false
                   }],
                   yAxes: [{
                       display: false
                   }]
               }
           }
       });

    var myChart2 = new Chart(ctx2, {
        type: 'line',
        data: data,
        options:
        {
            scales:
            {
                xAxes: [{
                    display: false
                }],
                yAxes: [{
                    display: false
                }]
            }
        }
    });



    var data2 = {
        labels: [
            "Red",
            "Blue",
            "Yellow"
        ],
        datasets: [
            {
                data: [300, 50, 100],
                backgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                    "#FFCE56"
                ],
                hoverBackgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                    "#FFCE56"
                ]
            }]
    };

    var myChart = new Chart(ctx3, {
        type: 'bar',
        data: {
            labels: ["", "", "", "", "", ""],
            datasets: [{
                label: '',
                data: [12, 19, 3, 5, 2, 3],
                backgroundColor: [
                    '#fff',
                    '#fff',
                    '#fff',
                    '#fff',
                    '#fff',
                    '#fff'
                ]
            }]
        },
        options:
        {
            scales:
            {
                xAxes: [{
                    display: false
                }],
                yAxes: [{
                    display: false
                }]
            }
        }
    });

    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["", "", "", "", "", ""],
            datasets: [{
                label: '',
                data: [12, 19, 3, 5, 2, 3],
                backgroundColor: [
                    '#fff',
                    '#fff',
                    '#fff',
                    '#fff',
                    '#fff',
                    '#fff'
                ]
            }]
        },
        options:
        {
            scales:
            {
                xAxes: [{
                    display: false
                }],
                yAxes: [{
                    display: false
                }]
            }
        }
    });
});
