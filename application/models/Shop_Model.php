<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shop_Model extends CI_Model {
    var $dbn = "";

    function __construct() {
        $ci =& get_instance();
        $ci->config->load('cs/config');
        $this->dbn = $ci->config->item('RanShop');
    }


    # ---------------------------
    #   sHOP PURCHASE
    #-----------------------------
    function insert_shoppurchase($data) {
        $this->db->insert($this->dbn . '.ShopPurchase', $data);
        return $this->db->insert_id();
    }

    //---------------------------
    // ITEM MAP
    // -------------------------
    function insert_itemmap($data) {
        $this->db->insert($this->dbn . '.ShopItemMap', $data);
        return $this->db->insert_id();
    }

    function update_itemmap($data, $product_num) {
        $this->db->where('ProductNum', $product_num);
        $this->db->update($this->dbn . '.ShopItemMap', $data);
        return $this->db->affected_rows();
    }

    function delete_itemmap($ProductNum) {
        $this->db->where('ProductNum', $ProductNum);
        $this->db->delete($this->dbn . '.ShopItemMap');
        return $this->db->affected_rows();
    }

    function get_all_items($itemsec = "1") {
        return $this->db->get_where($this->dbn . '.ShopItemMap', array("ItemSec" => $itemsec, "hidden" => 0 ))->result();
    }

    function get_all_items_ctg($ctg) {
        return $this->db->get_where($this->dbn . '.ShopItemMap', array("ItemCtg" => $ctg, "hidden" => 0 ))->result();
    }

    function get_all() {
        return $this->db->get($this->dbn . '.ShopItemMap')->result();
    }

    function get_item($product_num) {
        return $this->db->get_where($this->dbn . '.ShopItemMap', array("ProductNum" => $product_num))->row();
    }

    function get_search($itemsec, $keyword) {
        $this->db->where('ItemSec', $itemsec);
        $this->db->where('hidden', 0);
        $this->db->like('ItemName', $keyword);
        return $this->db->get($this->dbn . '.ShopItemMap')->result();
    }
}
