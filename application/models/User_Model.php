<?php
class User_Model extends CI_Model {
    var $dbn = "";
    var $dbGame1 = "";
    var $dbGame2 = "";

    function __construct() {
        $ci =& get_instance();
        $ci->config->load('cs/config');
        $this->dbn = $ci->config->item('RanUser');
        $this->dbGame1 = $ci->config->item('RanGame');
        $this->dbGame2 = $ci->config->item('RanGame2');
    }

  function insert_user($data) {
      $this->db->insert($this->dbn . '.UserInfo', $data);
      return $this->db->insert_id();
  }

  function get() {
      return $this->db->get($this->dbn . '.UserInfo')->result();
  }

  function get_username($username) {
      $this->db->where('UserName', $username);
      return $this->db->get($this->dbn . '.UserInfo')->row();
  }

  function get_usernum($usernum) {
      $this->db->where('UserNum', $usernum);
      return $this->db->get($this->dbn . '.UserInfo')->row();
  }

  function update_userinfo($username, $data) {
      $this->db->where('UserName', $username);
      $this->db->update($this->dbn.'.UserInfo', $data);
      return $this->db->affected_rows();
  }

  function get_email($email) {
      $this->db->where('UserEmail', $email);
      return $this->db->get($this->dbn . '.UserInfo')->row();
  }

  function get_ranking($server, $rank_type, $limit) {
        $server = $server == 1 ? $this->dbGame1 : $this->dbGame2;
        $this->db->select("P.ChaMoney, U.UserType,P.ChaName,P.ChaLevel,P.ChaLevelNew,P.ChaClass,P.ChaExp,P.ChaOnline,P.SWKill,P.CWKill,P.RhdmKill,P.SWDeath,P.CWDeath,P.RhdmDeath,P.GuNum,P.LevelUpDate", TRUE);
        $this->db->from($this->dbn . '.UserInfo as U');
        $this->db->join($server . '.ChaInfo as P', 'U.UserNum = P.UserNum');
        $this->db->where('U.UserType <' , '11');
        $this->db->where('P.ChaDeleted !=', '1');

        if($rank_type == "sw") {
            $this->db->order_by('P.SWKill DESC, P.SWDeath ASC');
        } else if($rank_type == "gd") {
            $this->db->order_by('P.ChaMoney DESC');
        } else if($rank_type == "pk") {
            $this->db->order_by("P.ChaPK2 DESC, P.ChaPKDeath ASC");
        } else if($rank_type == "lvl") {
            $this->db->order_by("P.ChaLevel DESC");
        } else if($rank_type == "cw") {
            $this->db->order_by("P.CWKill DESC, P.CWDeath ASC");
        }

        $this->db->limit($limit);
        $q = $this->db->get();
        return $q->result();
  }

  function get_ranking_top($server, $limit, $class) {
        $server = $server == 1 ? $this->dbGame1 : $this->dbGame2;
        $this->db->select("P.ChaName,P.ChaLevel,P.ChaMoney,P.ChaClass,P.ChaExp,P.ChaOnline,P.SWKill,P.CWKill,P.RhdmKill,P.SWDeath,P.CWDeath,P.RhdmDeath,P.GuNum", TRUE);
        $this->db->from($this->dbn . '.UserInfo as U');
        $this->db->join($server . '.ChaInfo as P', 'U.UserNum = P.UserNum');
        $this->db->where('U.UserType <' , '11');
        $this->db->where('P.ChaDeleted !=', '1');
        $this->db->where_in('P.ChaClass', $class);
        $this->db->order_by("(P.SWKill+P.CWKill+P.RhdmKill) DESC, (P.SWDeath+P.CWDeath+P.RhdmDeath) ASC, P.ChaLevel DESC, P.ChaNum ASC");
        $this->db->limit($limit);
        $q = $this->db->get();

        if($limit == 1) {
            return $q->row();
        } else {
            return $q->result();
        }
  }

  /*
  SELECT TOP 1 P.ChaName,P.ChaLevel,P.ChaClass,P.ChaExp,P.ChaOnline,P.SWKill,P.CWKill,P.RhdmKill,P.SWDeath,P.CWDeath,P.RhdmDeath,P.GuNum
  FROM $dfsql[db4].dbo.UserInfo U, $randb.dbo.ChaInfo P
  WHERE P.UserNum = U.UserNum
  AND U.UserType < 11
  AND P.ChaDeleted != 1
  AND P.ChaClass IN (1,64)
   ORDER BY (P.SWKill+P.CWKill+P.RhdmKill) DESC,
   (P.SWDeath+P.CWDeath+P.RhdmDeath) ASC,P.ChaLevel DESC
  */

}
