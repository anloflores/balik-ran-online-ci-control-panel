<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function index() {
		if($this->session->userdata('UserName') === null) {
			redirect(base_url());
			exit();
		}

		$data['page_title'] = "Home";
		$this->load->view('home_view', $data);
	}

	public function topup() {
		if($this->session->userdata('UserName') === null) {
			redirect(base_url());
			exit();
		}

		$this->form_validation->set_rules('code','Card Code','required');
		$this->form_validation->set_rules('pincode','Card Pin Code','required|callback_check_code');

		if($this->form_validation->run()) {
			$this->session->set_flashdata('top_err', '<div class="success-holder"><p>Successfully Top Uped!</p></div>');

			$topup = $this->CP_Model->get_code($this->cf->hash($this->input->post('code')));

			$total_points = $topup->Amount + $this->CP_Model->get_points($this->session->userdata('UserName'))->Points;
			$points_data = array(
				"Points" => $total_points
			);

			$record_data = array(
				"UserName" => $this->session->userdata('UserName'),
				"Code" => $this->input->post('code'),
				"Pin" => $this->input->post('pincode'),
				"Amount" => $topup->Amount,
				"LValue" => $this->CP_Model->get_points($this->session->userdata('UserName'))->Points,
				"NValue" => $total_points
			);

			$this->CP_Model->insert_topup_record($record_data);
			$this->CP_Model->update_points($points_data, $this->session->userdata('UserName'));
			$this->CP_Model->delete_topup($this->cf->hash($this->input->post('code')), $this->cf->hash($this->input->post('pincode')));

			$activity_data = array(
				"description" => "Success Topup",
				"userid" => $this->User_Model->get_username($this->session->userdata('UserName'))->UserNum,
				"type" => 1
			);
			$this->CP_Model->insert_activity($activity_data);

		} else {
			$activity_data = array(
				"description" => "Failed Topup",
				"userid" => $this->User_Model->get_username($this->session->userdata('UserName'))->UserNum,
				"type" => 1
			);
			$this->CP_Model->insert_activity($activity_data);
			$this->session->set_flashdata('top_err', '<div class="error-holder">'. validation_errors('<p>', '</p>') .'</div>');
		}

		$data['page_title'] = "User - Topup";
		$this->load->view('user/topup_view', $data);
	}

	public function changepassword() {
		if($this->session->userdata('UserName') === null) {
			redirect(base_url());
			exit();
		}

		$this->form_validation->set_rules('old', 'Old Password', 'required|callback_check_oldpassword');
		$this->form_validation->set_rules('new', 'New Password', 'required');
		$this->form_validation->set_rules('newr', 'Re New Password', 'required|matches[new]');

		if($this->form_validation->run()) {
			$this->session->set_flashdata('cperr', '<div class="success-holder"><p>Successfully Changed your Password</p></div>');

			$password_data = array(
				"UserPass" => $this->cf->hash($this->input->post('new'))
			);

			$activity_data = array(
				"description" => "Success Change Password",
				"userid" => $this->User_Model->get_username($this->session->userdata('UserName'))->UserNum,
				"type" => 1
			);
			$this->CP_Model->insert_activity($activity_data);
			$this->User_Model->update_userinfo($this->session->userdata('UserName'), $password_data);

		} else {
			$activity_data = array(
				"description" => "Failed Change Password",
				"userid" => $this->User_Model->get_username($this->session->userdata('UserName'))->UserNum,
				"type" => 1
			);
			$this->CP_Model->insert_activity($activity_data);
			$this->session->set_flashdata('cperr', '<div class="error-holder">'. validation_errors('<p>', '</p>') .'</div>');
		}

		$data['page_title'] = "User - Change Password";
		$this->load->view('user/changepassword_view', $data);
	}

	public function changepincode() {
		if($this->session->userdata('UserName') === null) {
			redirect(base_url());
			exit();
		}

		$this->form_validation->set_rules('old', 'Old Pin Code', 'required|callback_check_oldpin');
		$this->form_validation->set_rules('new', 'New Pin Code', 'required');
		$this->form_validation->set_rules('newr', 'Re New Pin Code', 'required|matches[new]');

		if($this->form_validation->run()) {
			$this->session->set_flashdata('cpcerr', '<div class="success-holder"><p>Successfully Changed your Pin Code</p></div>');

			$password_data = array(
				"UserPass2" => $this->cf->hash($this->input->post('new'))
			);

			$this->User_Model->update_userinfo($this->session->userdata('UserName'), $password_data);
			$activity_data = array(
				"description" => "Success Change Pin Code",
				"userid" => $this->User_Model->get_username($this->session->userdata('UserName'))->UserNum,
				"type" => 1
			);
			$this->CP_Model->insert_activity($activity_data);
			redirect(base_url() . 'user/changepincode/');

		} else {
			$activity_data = array(
				"description" => "Failed Change Pincode",
				"userid" => $this->User_Model->get_username($this->session->userdata('UserName'))->UserNum,
				"type" => 1
			);
			$this->CP_Model->insert_activity($activity_data);
			$this->session->set_flashdata('cpcerr', '<div class="error-holder">'. validation_errors('<p>', '</p>') .'</div>');
		}

		$data['page_title'] = "User - Change Pin Code";
		$this->load->view('user/changepincode_view', $data);
	}

	public function changeemail() {
		if($this->session->userdata('UserName') === null) {
			redirect(base_url());
			exit();
		}

		$this->form_validation->set_rules('old', 'Old Email', 'required|max_length[50]|valid_email|callback_check_email');
		$this->form_validation->set_rules('new', 'New Email', 'required|max_length[50]|valid_email');
		$this->form_validation->set_rules('newr', 'Re New Email', 'required|valid_email|matches[new]');

		if($this->form_validation->run()) {
			$this->session->set_flashdata('cerr', '<div class="success-holder"><p>Successfully Changed your Email Address to '. $this->input->post('new') .'</p></div>');

			$password_data = array(
				"UserEmail" => $this->input->post('new')
			);

			$this->User_Model->update_userinfo($this->session->userdata('UserName'), $password_data);

			$activity_data = array(
				"description" => "Success Change Email",
				"userid" => $this->User_Model->get_username($this->session->userdata('UserName'))->UserNum,
				"type" => 1
			);
			$this->CP_Model->insert_activity($activity_data);
		} else {
			$activity_data = array(
				"description" => "Failed Change Email",
				"userid" => $this->User_Model->get_username($this->session->userdata('UserName'))->UserNum,
				"type" => 1
			);
			$this->CP_Model->insert_activity($activity_data);
			$this->session->set_flashdata('cerr', '<div class="error-holder">'. validation_errors('<p>', '</p>') .'</div>');
		}

		$data['page_title'] = "User - Change Email Address";
		$this->load->view('user/changeemail_view', $data);
	}

	public function eptvp() {
		if($this->session->userdata('UserName') === null) {
			redirect(base_url());
			exit();
		}

		$this->form_validation->set_rules('epoints', 'E-Points', 'required|integer|greater_than[19]|callback_check_epoints');
		if($this->form_validation->run()) {
			$this->session->set_flashdata('eptvp_err', '<div class="success-holder"><p>You\'ve Successfully Converted E-Points to V-Points</p></div>');

			$n_points = intval($this->CP_Model->get_points($this->session->userdata['UserName'])->Points) - intval($this->input->post('epoints'));
			$n_vpoints = intval($this->CP_Model->get_points($this->session->userdata['UserName'])->VPoints) + intval($this->input->post('epoints') * (int)$this->config->item('ep_to_vp'));

			$this->CP_Model->update_points(array("Points" => $n_points), $this->session->userdata['UserName']);
			$this->CP_Model->update_points(array("VPoints" => $n_vpoints), $this->session->userdata['UserName']);
			$this->CP_Model->insert_covert_record(array("UserName" => $this->session->userdata['UserName'], "Amount" => $this->input->post('epoints'), "Type" => "E-Points To V-Points", "OPoints" => $this->CP_Model->get_points($this->session->userdata['UserName'])->VPoints, "NPoints" => $n_vpoints));

			$activity_data = array(
				"description" => "Success Convertion From EP to VP",
				"userid" => $this->User_Model->get_username($this->session->userdata('UserName'))->UserNum,
				"type" => 1
			);
			$this->CP_Model->insert_activity($activity_data);

		} else {
			$activity_data = array(
				"description" => "Failed Convertion From EP to VP",
				"userid" => $this->User_Model->get_username($this->session->userdata('UserName'))->UserNum,
				"type" => 1
			);
			$this->CP_Model->insert_activity($activity_data);
			$this->session->set_flashdata('eptvp_err', '<div class="error-holder">'. validation_errors('<p>', '</p>') .'</div>');
		}

		$data['page_title'] = "User - Convert EPoints to VPoints";
		$this->load->view('user/eptvp_view', $data);
	}

	public function gttovp() {
		if($this->session->userdata('UserName') === null) {
			redirect(base_url());
			exit();
		}

		$u = $this->User_Model->get_username($this->session->userdata['UserName']);
		$PTotal = (($u->GameTime/60)/(int)$this->CP_Model->get_setting('gttvp')->value);

		if($this->input->post('convert') !== null) {
			$current = $u->GameTime - ($PTotal*60*(int)$this->config->item('gt_to_vp'));
			$nVPoints = $PTotal + intval($this->CP_Model->get_points($this->session->userdata['UserName'])->VPoints);
			$this->User_Model->update_userinfo($this->session->userdata['UserName'], array("GameTime" => $current));
			$this->CP_Model->update_points(array("VPoints" => $nVPoints), $this->session->userdata['UserName']);

			$this->CP_Model->insert_covert_record(array("UserName" => $this->session->userdata['UserName'], "Amount" => $PTotal, "Type" => "Game Time to V-Points", "OPoints" => $this->CP_Model->get_points($this->session->userdata['UserName'])->VPoints, "NPoints" => $nVPoints));
			$this->session->set_flashdata('gttovp_err', "<div class='success-holder'><p>Successfully Converted Game Time to V-Points. You can now use the converted points.</p></div>");
			$activity_data = array(
				"description" => "Success Convertion Game Time to VP",
				"userid" => $this->User_Model->get_username($this->session->userdata('UserName'))->UserNum,
				"type" => 1
			);
			$this->CP_Model->insert_activity($activity_data);

			$PTotal = (($current/60)/(int)$this->CP_Model->get_setting('gttvp')->value);
		}

		$data['time_avail'] = $PTotal;
		$data['page_title'] = "User - Convert Game Time to V Points";
		$this->load->view('user/gttovp_view', $data);
	}

	public function chadisplay() {
		if($this->session->userdata('UserName') === null) {
			redirect(base_url());
			exit();
		}

		$data['page_title'] = "User - Character Display";
		$this->load->view('user/chadisplay_view', $data);
	}

	public function changeschool($school = "strife") {
		if($this->session->userdata('UserName') === null) {
			redirect(base_url());
			exit();
		}

		if($school == "strife") {
			$data['server'] = "Strife";
			$this->Game_Model->set_dbn($this->config->item('RanGame'));
		} else {
			$data['server'] = "Havoc";
			$this->Game_Model->set_dbn($this->config->item('RanGame2'));
		}

		$this->form_validation->set_rules('char', 'Character', 'required|integer');
		$this->form_validation->set_rules('school', 'School', 'required|integer');

		if($this->form_validation->run()) {
			$v_fee = $this->config->item('change_school')[0];
			$v_type = $this->config->item('change_school')[0];

			if($v_type == 1) {
				if($this->CP_Model->get_points($this->session->userdata['UserName'])->Points >= $v_fee) {
					$n_vp = $this->CP_Model->get_points($this->session->userdata['UserName'])->Points - $v_fee;
					$vp_data = array(
						"Points" => $n_vp
					);
					$this->CP_Model->update_points($vp_data, $this->session->userdata['UserName']);

					$cha_info = array(
						"ChaSchool" => $this->input->post('school')
					);
					$this->Game_Model->update_chainfo($this->input->post('char'), $cha_info);
					$this->session->set_flashdata('cs_err','<div class="success-holder"><p>Successfully Changed School.</p></div>');
					$activity_data = array(
						"description" => "Success Change School",
						"userid" => $this->User_Model->get_username($this->session->userdata('UserName'))->UserNum,
						"type" => 1
					);
					$this->CP_Model->insert_activity($activity_data);
				} else {
					$this->session->set_flashdata('cs_err','<div class="error-holder"><p>Insufficient Vote Points.</p></div>');
				}
			} else {
				if($this->CP_Model->get_points($this->session->userdata['UserName'])->VPoints >= $v_fee) {
					$n_vp = $this->CP_Model->get_points($this->session->userdata['UserName'])->VPoints - $v_fee;
					$vp_data = array(
						"VPoints" => $n_vp
					);
					$this->CP_Model->update_points($vp_data, $this->session->userdata['UserName']);

					$cha_info = array(
						"ChaSchool" => $this->input->post('school')
					);
					$this->Game_Model->update_chainfo($this->input->post('char'), $cha_info);
					$this->session->set_flashdata('cs_err','<div class="success-holder"><p>Successfully Changed School.</p></div>');
					$activity_data = array(
						"description" => "Success Change School",
						"userid" => $this->User_Model->get_username($this->session->userdata('UserName'))->UserNum,
						"type" => 1
					);
					$this->CP_Model->insert_activity($activity_data);
				} else {
					$this->session->set_flashdata('cs_err','<div class="error-holder"><p>Insufficient Vote Points.</p></div>');
				}
			}
		} else {
			$this->session->set_flashdata('cs_err', '<div class="error-holder">'. validation_errors('<p>', '</p>') .'</div>');
		}

		$data['school'] = $school;
		$data['page_title'] = "User - Change School";
		$this->load->view('user/changeschool_view', $data);
	}

	public function isrecord() {
		if($this->session->userdata('UserName') === null) {
			redirect(base_url());
			exit();
		}

		$data['page_title'] = "Item Shop Record";
		$this->load->view('user/itemshop_rec_view', $data);
	}

	public function crecord() {
		if($this->session->userdata('UserName') === null) {
			redirect(base_url());
			exit();
		}

		$data['page_title'] = "Covert Record";
		$this->load->view('user/convert_rec_view', $data);
	}

	public function trecord() {
		if($this->session->userdata('UserName') === null) {
			redirect(base_url());
			exit();
		}

		$data['page_title'] = "Topup Record";
		$this->load->view('user/topup_rec_view', $data);
	}

	/*
		----------------------
		EPOINTS VALIDATION
		----------------------
	*/

	function check_epoints($points) {
		if($points <= $this->CP_Model->get_points($this->session->userdata['UserName'])->Points) {
			return true;
		} else {
			$this->form_validation->set_message('check_epoints', "You don't have enough E-Points.");
			return false;
		}
	}

	/*
		-----------------------
			CHANGE PASSWORD VALIDATION
		-----------------------
	*/

	function check_oldpassword($password) {
		$password = $this->cf->hash($password);
		$u = $this->User_Model->get_username($this->session->userdata('UserName'));

		if($u->UserPass == $password) {
			return true;
		} else {
			$this->form_validation->set_message('check_oldpassword', "Current password doesn't match to your actual current password.");
			return false;
		}
	}

	function check_oldpin($password) {
		$password = $this->cf->hash($password);
		$u = $this->User_Model->get_username($this->session->userdata('UserName'));

		if($u->UserPass2 == $password) {
			return true;
		} else {
			$this->form_validation->set_message('check_oldpin', "Current Pin Code doesn't match to your actual current Pin Code.");
			return false;
		}
	}

	function check_email($email) {
		$u = $this->User_Model->get_username($this->session->userdata('UserName'));

		if($u->UserEmail == $email) {
			return true;
		} else {
			$this->form_validation->set_message('check_email', "Current Email entered doesn't match to actual current email.");
			return false;
		}
	}

	/*

	-------------------------------
		TOP UP VALIDATION FUNCTIONS
	-------------------------------

	*/

	function check_code($pin) {
		$pin = $this->cf->hash($pin);
		$code = $this->cf->hash($this->input->post('code'));

		if($this->CP_Model->get_code($code)) {
			$r = $this->CP_Model->get_code($code);
			if($r->Pin == $pin) {
				return true;
			} else {
				$this->form_validation->set_message('check_code', "Code & Pin Doesn't Match.");
				return false;
			}
		} else {
			$this->form_validation->set_message('check_code', "Card Code Doesn't Exists.");
			return false;
		}
	}

}
