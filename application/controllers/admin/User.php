<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function index() {

        if($this->session->userdata('admin') == null) {
            redirect(base_url() . 'admin/');
            exit();
        }

        $s = $this->session->userdata('admin');

        $this->form_validation->set_rules('fname', 'First name', 'required');
        $this->form_validation->set_rules('lname', 'Last name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('opass', 'Old Password', '');
        $this->form_validation->set_rules('npass', 'New Password', '');
        $this->form_validation->set_rules('rnpass', 'Re New Password', '');

        if($this->form_validation->run()) {

            $user_data['fname'] = $this->input->post('fname');
            $user_data['lname'] = $this->input->post('lname');
            $user_data['email'] = $this->input->post('email');

            if(!empty($this->input->post('opass'))
            || !empty($this->input->post('npass'))
            || !empty($this->input->post('rnpass'))) {

                $i = $this->CP_Model->get_admin($s['u'], $this->cf->hash($s['p']));

                if($i->password == $this->cf->hash($this->input->post('opass'))
                && $this->input->post('npass') == $this->input->post('rnpass')) {

                    $user_data['password'] = $this->cf->hash($this->input->post('npass'));
                    $admin_data = array(
                        "u" => $i->username,
                        "p" => $this->cf->hash($this->input->post('npass'))
                    );
                    $this->session->set_userdata('admin', $admin_data);

                } else {
                    $err_data = array(
                        "status" => 0,
                        "error" => "Error! Old Password isn't equal to current password."
                    );

                    $this->session->set_flashdata('err', $err_data);
                }
            }

            $this->CP_Model->update_admin($s['u'], $this->cf->hash($s['p']), $user_data);

            $err_data = array(
                "status" => 1,
                "error" => "Successfully Updated your Info."
            );

            $this->session->set_flashdata('err', $err_data);
        } else {
            $err_data = array(
                "status" => 0,
                "error" => validation_errors('<p>', '</p>')
            );

            $this->session->set_flashdata('err', $err_data);
        }

        $data['u'] = $this->CP_Model->get_admin($s['u'], $this->cf->hash($s['p']));
        $data['page_title'] = "User Information";
        $this->load->view('admin/user_info_view', $data);

    }

    public function create() {
        if($this->session->userdata('admin') == null) {
            redirect(base_url() . 'admin/');
            exit();
        }

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('fname', 'First name', 'required');
        $this->form_validation->set_rules('lname', 'Last name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('pass', 'Password', 'required');
        $this->form_validation->set_rules('rpass', 'Re Password', 'required|matches[pass]');

        if($this->form_validation->run()) {
            $admin_data = array(
                "username" => $this->input->post('username'),
                "fname" => $this->input->post('fname'),
                "lname" => $this->input->post('lname'),
                "email" => $this->input->post('email'),
                "password" => $this->cf->hash($this->input->post('pass'))
            );

            $this->CP_Model->insert_admin($admin_data);

            $err_data = array(
                "status" => 1,
                "error" => "Successfully Created New User."
            );

            $this->session->set_flashdata('err', $err_data);

        } else {
            $err_data = array(
                "status" => 0,
                "error" => validation_errors('<p>', '</p>')
            );

            $this->session->set_flashdata('err', $err_data);
        }

        $data['page_title'] = "Create User";
        $this->load->view('admin/add_user_view', $data);
    }

}
