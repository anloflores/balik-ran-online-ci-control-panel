<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Topup extends CI_Controller {

    public function index() {

        if($this->session->userdata('admin') == null) {
            redirect(base_url() . 'admin/');
            exit();
        }

        $data['page_title'] = "Top Up";
        $this->load->view('admin/all_topup_view', $data);

    }


    public function generate() {

        if($this->session->userdata('admin') == null) {
            redirect(base_url() . 'admin/');
            exit();
        }

        $this->form_validation->set_rules('code', 'Code', 'required|max_length[50]');
        $this->form_validation->set_rules('pin', 'Pin', 'required|max_length[50]|callback_check_topup');
        $this->form_validation->set_rules('amount', 'Amount', 'required');

        if($this->form_validation->run()) {

            $topup_data = array(
                "Code" => $this->cf->hash($this->input->post('code')),
                "Pin" => $this->cf->hash($this->input->post('pin')),
                "Amount" => (int)$this->input->post('amount')
            );

            $this->CP_Model->insert_topup($topup_data);

            $err_data = array(
                "status" => 1,
                "error" => "You've successfully generated topup."
            );

            $this->session->set_flashdata('err', $err_data);
        } else {
            $err_data = array(
                "status" => 0,
                "error" => validation_errors('<p>', '</p>')
            );

            $this->session->set_flashdata('err', $err_data);
        }

        $data['page_title'] = "Generate Top Up";
        $this->load->view('admin/add_topup_view', $data);

    }

    function check_topup($pin) {
        $pin = $this->cf->hash($pin);
        $code = $this->cf->hash($this->input->post('code'));

        if($this->CP_Model->check_topup($code, $pin) == null) {
            return true;
        } else {
            $this->form_validation->set_message('check_topup', "Top Up Code and Pin is already exists.");
            return false;
        }

    }
}
