<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index() {

        if($this->session->userdata('admin') !== null) {
            redirect(base_url() . 'admin/dashboard/');
            exit();
        }

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required|callback_admin_check');

        if($this->form_validation->run()) {
            redirect(base_url() . 'admin/dashboard/');
        } else {
            $err_data = array(
                "status" => 0,
                "error" => validation_errors('<p>', '</p>')
            );

            $this->session->set_flashdata('log_err', $err_data);
        }

        $data['page_title'] = "Login";
        $this->load->view('admin/login_view', $data);
    }

    public function logout() {
		session_destroy();
		redirect(base_url() . 'admin/');
    }

    function admin_check($password) {
        $username = $this->input->post('username');
        $hashed = $this->cf->hash($password);
        $a = $this->CP_Model->get_admin($username, $hashed);

        if($a !== null) {
            $admin_data = array(
                "u" => $username,
                "p" => $password
            );
            $this->session->set_userdata('admin', $admin_data);
            return true;
        } else {
            $this->form_validation->set_message('admin_check', "Account doesn't exists.");
            return false;
        }

    }

}
