<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

    public function index() {

        if($this->session->userdata('admin') == null) {
            redirect(base_url() . 'admin/');
            exit();
        }

        $data['page_title'] = "News";
        $this->load->view('admin/all_news_view', $data);

    }

    public function add() {
        if($this->session->userdata('admin') == null) {
            redirect(base_url() . 'admin/');
            exit();
        }

        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('type', 'Type', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('tags', 'Tags', 'required');
        $s = $this->session->userdata('admin');
        $u = $this->CP_Model->get_admin($s['u'], $this->cf->hash($s['p']));

        if($this->form_validation->run()) {
            $news_data = array(
                "title" => $this->input->post('title'),
                "type" => $this->input->post('type'),
                "description" => $this->input->post('description'),
                "status" => $this->input->post('publish') !== null ? '1' : '0',
                "tags" => $this->input->post('tags'),
                "userid" => $u->userid
            );

            $this->CP_Model->insert_news($news_data);
            $err_data = array(
                "status" => 1,
                "error" => "Successfully added news."
            );

            $this->session->set_flashdata('err', $err_data);
        } else {
            $err_data = array(
                "status" => 0,
                "error" => validation_errors('<p>', '</p>')
            );

            $this->session->set_flashdata('err', $err_data);
        }

        $data['page_title'] = "Add News";
        $this->load->view('admin/add_news_view', $data);
    }

    public function edit($newsid) {
        if($this->session->userdata('admin') == null) {
            redirect(base_url() . 'admin/');
            exit();
        }
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('type', 'Type', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('tags', 'Tags', 'required');
        $s = $this->session->userdata('admin');
        $u = $this->CP_Model->get_admin($s['u'], $this->cf->hash($s['p']));

        if($this->form_validation->run()) {
            $news_data = array(
                "title" => $this->input->post('title'),
                "type" => $this->input->post('type'),
                "description" => $this->input->post('description'),
                "status" => $this->input->post('publish') !== null ? '1' : '0',
                "tags" => $this->input->post('tags')
            );

            $this->CP_Model->update_news($newsid, $news_data);
            $err_data = array(
                "status" => 1,
                "error" => "Successfully edited news."
            );

            $this->session->set_flashdata('err', $err_data);
        } else {
            $err_data = array(
                "status" => 0,
                "error" => validation_errors('<p>', '</p>')
            );

            $this->session->set_flashdata('err', $err_data);
        }

        $data['news'] = $this->CP_Model->get_news_id($newsid);
        $data['page_title'] = "Edit News #" . $newsid;
        $this->load->view('admin/edit_news_view', $data);
    }

    public function delete($newsid) {
        if($this->session->userdata('admin') == null) {
            redirect(base_url() . 'admin/');
            exit();
        }

        $this->CP_Model->delete_news($newsid);
        $this->CP_Model->update_news($newsid, $news_data);
        $err_data = array(
            "status" => 1,
            "error" => "Successfully Deleted News."
        );

        $this->session->set_flashdata('err', $err_data);
        redirect(base_url() . 'admin/news/');
    }

}
