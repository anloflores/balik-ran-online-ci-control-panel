<?php
    $this->load->view('inc/head_view');
    $this->load->view('inc/nav_view');
?>


    <!-- Main Content -->
    <div class="container_12">
        <!-- Content -->
        <div class="grid_8">
            <div class="panel-container">
                <div class="panel-header">
                    <h1>Account Registration</h1>
                </div>

                <div class="panel-content">
                    <center><?=$this->session->flashdata('reg_err')?></center>

                    <form action="" method="post">
                        <div class="form-panel">
                            <div class="form-panel-head"><h1>User Account Infomation</h1></div>
                        </div>

                        <div class="form-group">
                            <div class="col_4">
                                <label for="username">Username</label>
                                <input id="username" name="username" class="full" value="<?=set_value('username')?>" type="text">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col_2">
                                <label for="password">Password</label>
                                <input id="password" name="password" class="full" value="<?=set_value('password')?>" type="password">
                            </div>

                            <div class="col_2">
                                <label for="re-password">Re-Password</label>
                                <input id="re-password" name="re_password" class="full" value="<?=set_value('password')?>" type="password">
                            </div>
                        </div>

                        <br/>
                        <div class="form-panel">
                            <div class="form-panel-head"><h1>Recovery Infomation</h1></div>
                        </div>

                        <div class="form-group">
                            <div class="col_4">
                                <label for="pin">Pin</label>
                                <input id="pin" class="full" type="password" value="<?=set_value('pin')?>" name="pin">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col_4">
                                <label for="email">Email</label>
                                <input id="email" class="full" type="text" value="<?=set_value('email')?>" name="email">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col_4">
                                <label for="secret-question">Secret Question</label>
                                <select name="secret_question" class="full" id="secret-question">
                                    <option selected="selected" value="">-</option>
    								<option value="1" <?=set_value('secret_question') == 1 ? 'selected' : ''?>>What is the first name of your favorite uncle?</option>
    								<option value="2" <?=set_value('secret_question') == 2 ? 'selected' : ''?>>Where did you meet your spouse?</option>
    								<option value="3" <?=set_value('secret_question') == 3 ? 'selected' : ''?>>What is your oldest cousin's name?</option>
    								<option value="4" <?=set_value('secret_question') == 4 ? 'selected' : ''?>>What is your youngest child's nickname?</option>
    								<option value="5" <?=set_value('secret_question') == 5 ? 'selected' : ''?>>What is your oldest child's nickname?</option>
    								<option value="6" <?=set_value('secret_question') == 6 ? 'selected' : ''?>>What is the first name of your oldest niece?</option>
    								<option value="7" <?=set_value('secret_question') == 7 ? 'selected' : ''?>>What is the first name of your oldest nephew?</option>
    								<option value="8" <?=set_value('secret_question') == 8 ? 'selected' : ''?>>What is the first name of your favorite aunt?</option>
    								<option value="9" <?=set_value('secret_question') == 9 ? 'selected' : ''?>>Where did you spend your honeymoon?</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col_4">
                                <label for="secret-answer">Secret Answer</label>
                                <input id="secret-answer" name="secret_answer" value="<?=set_value('secret_answer')?>" class="full" type="text">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col_1">
                                <label for="captcha">Captcha</label>
                                <input id="captcha" name="captcha" class="full" type="text">
                            </div>

                            <div class="col_3">
                                <?=$cap?>
                            </div>
                        </div>

                        <center>
                            <input type="submit" value="Register">
                        </center>
                    </form>
                </div>
            </div>
        </div>

        <!-- Side Bar -->
        <div class="grid_4">
            <!-- login -->
            <?php $this->load->view('mod/panel_login_view') ?>
            <?php $this->load->view('mod/panel_ranking_view') ?>
        </div>
    </div>

    <?php $this->load->view('inc/footer_view') ?>

</body>
<script src="<?=base_url()?>assets/js/jquery-1.11.0.min.js" charset="utf-8"></script>
<script type="text/javascript">
    $(function() {
        // Image Slider
            var _slider = $('.image-slider ul');
            var _count = $('li[data-image]').length;
            var _ctr = 1;
            var _rotateRate = 3000;

            var rot = setInterval(function() {
                if(_ctr != _count) {
                      $('li[data-image]:nth-child('+ _ctr +')').fadeOut("slow");
                      _ctr++;
                      $('li[data-image]:nth-child('+ _ctr +')').fadeIn("slow");

                      activate_bullet(_ctr);
                } else {
                      $('li[data-image]:nth-child(1)').fadeIn("slow");
                      $('li[data-image]:nth-child('+ _count +')').fadeOut("slow");
                      _ctr = 1;

                      activate_bullet(_ctr);
                }
            }, _rotateRate);

            $("li[data-image]").each(function(k, v) {
                var ch = "";

                if(k == 0)
                    ch = "active";

                var bg = $(this).attr('data-image');
                $(this).css('background-image', 'url('+ bg +')');
                $('ul.slider-bullet').append('<li class="slider-bullet-item '+ ch +'"></li>');
            });

            $(document).on('click', '.slider-bullet-item', function() {
                var i = $(this).index()+1;
                $('li[data-image]:nth-child('+ i +')').fadeIn("slow");
                $('li[data-image]:nth-child('+ _ctr +')').fadeOut("fast");
                _ctr = i;
                activate_bullet(_ctr);
            });
            function activate_bullet(ctr) {
                $('.slider-bullet-item').removeClass('active');
                $('.slider-bullet-item:nth-child('+ ctr +')').addClass('active');
            }

        // End Image Slider


        // Tab-Pane
        $('a[data-activate-id]').click(function() {
            var id = $(this).attr('data-activate-id');
            var type = $(this).attr('data-tab-type');



            $(".tab-pane[id!='"+id+"'][data-tab-type='"+type+"']").fadeOut('fast');
            $("#"+ id +"").delay(200).fadeIn('slow');

            return false;
        });
    });
</script>

</html>
