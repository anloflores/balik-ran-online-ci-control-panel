<?php
    $this->load->view('inc/head_view');
    $this->load->view('inc/nav_view');
?>


    <!-- Main Content -->
    <div class="container_12">
        <!-- Content -->
        <div class="grid_8">
            <!--
                Image Slider - You can add a text content
                Just add inline style to customize content
            -->
            <div class="image-slider">
                <ul>
                    <li class="image-slider-content" data-image="<?=base_url()?>assets/img/slider/sample1.jpg">
                        <h1 style="color: #fff; margin: 0; text-shadow: 0px 0px 10px rgba(0, 0, 0, 0.6);">
                            Welcome to Balik Ran
                        </h1>
                        <p style="color: #fff; text-shadow: 0px 0px 10px rgba(0, 0, 0, 0.6);">
                            Experience the Thrill, Experience the Awesomeness
                        </p>
                    </li>
                    <li class="image-slider-content" data-image="<?=base_url()?>assets/img/slider/sample2.jpg">
                        <h1 style="color: #fff; margin: 0; text-shadow: 0px 0px 10px rgba(0, 0, 0, 0.6);">
                            You can add more! All Items are capable.
                        </h1>
                    </li>
                    <li class="image-slider-content" data-image="<?=base_url()?>assets/img/slider/sample3.jpg"> </li>
                    <li class="image-slider-content" data-image="<?=base_url()?>assets/img/slider/sample4.jpg"> </li>
                </ul>

                <div class="bullet-holder">
                    <center>
                        <ul class="slider-bullet"></ul>
                    </center>
                </div>
            </div>

            <!-- End of Image Slider -->
            <Br/><Br/>
            <!-- Start of News Content -->
            <div class="panel-container">
                <div class="panel-header">
                    <h1>News</h1>
                </div>

                <div class="panel-content">
                    <center>
                        <span class="option" data-sort="0">
                            All News
                        </span>
                        <span class="option separate">/</span>
                        <span class="option" data-sort="1">
                            Promotion
                        </span>
                        <span class="option separate">/</span>
                        <span class="option" data-sort="2">
                            Rankings
                        </span>
                        <span class="option separate">/</span>
                        <span class="option" data-sort="3">
                            Guides
                        </span>
                        <span class="option separate">/</span>
                        <span class="option" data-sort="4">
                            System
                        </span>
                        <span class="option separate">/</span>
                        <span class="option" data-sort="5">
                            News
                        </span>
                    </center>

                    <table class="news-holder">
                        <?php foreach ($this->CP_Model->get_news_front() as $key => $var): ?>
                            <?php
                                switch ($var->type) {
                                    case 1: $type = '<span class="label bolder red">Promo</span>'; break;
                                    case 2: $type = '<span class="label bolder orange">Rankings</span>'; break;
                                    case 3: $type = '<span class="label bolder green">Guides</span>'; break;
                                    case 4: $type = '<span class="label bolder yellow">System</span>'; break;
                                    case 5: $type = '<span class="label bolder blue">News</span>'; break;
                                }
                             ?>
                            <tr style="cursor: pointer;" data-type="<?=$var->type?>" onclick="location.href = '<?=base_url()?>home/news/<?=$var->newsid?>/';">
                                <td class="news-type"><?=$type?></td>
                                <td class="news-title"><?=$var->title?></td>
                                <td class="news-date"><?=substr($var->date_created,0, 11)?></td>
                                <td class="news-user"><?=$this->CP_Model->get_admin_userid($var->userid)->username?></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>

        <!-- Side Bar -->
        <div class="grid_4">
            <!-- login -->
            <?php $this->load->view('mod/panel_login_view') ?>
            <?php $this->load->view('mod/panel_ranking_view') ?>
        </div>
    </div>

    <?php $this->load->view('inc/footer_view') ?>

</body>
<script src="<?=base_url()?>assets/js/jquery-1.11.0.min.js" charset="utf-8"></script>
<script type="text/javascript">
    $(function() {
        // Image Slider
            var _slider = $('.image-slider ul');
            var _count = $('li[data-image]').length;
            var _ctr = 1;
            var _rotateRate = 3000;

            var rot = setInterval(function() {
                if(_ctr != _count) {
                      $('li[data-image]:nth-child('+ _ctr +')').fadeOut("slow");
                      _ctr++;
                      $('li[data-image]:nth-child('+ _ctr +')').fadeIn("slow");

                      activate_bullet(_ctr);
                } else {
                      $('li[data-image]:nth-child(1)').fadeIn("slow");
                      $('li[data-image]:nth-child('+ _count +')').fadeOut("slow");
                      _ctr = 1;

                      activate_bullet(_ctr);
                }
            }, _rotateRate);

            $("li[data-image]").each(function(k, v) {
                var ch = "";

                if(k == 0)
                    ch = "active";

                var bg = $(this).attr('data-image');
                $(this).css('background-image', 'url('+ bg +')');
                $('ul.slider-bullet').append('<li class="slider-bullet-item '+ ch +'"></li>');
            });

            $(document).on('click', '.slider-bullet-item', function() {
                var i = $(this).index()+1;
                $('li[data-image]:nth-child('+ i +')').fadeIn("slow");
                $('li[data-image]:nth-child('+ _ctr +')').fadeOut("fast");
                _ctr = i;
                activate_bullet(_ctr);
            });
            function activate_bullet(ctr) {
                $('.slider-bullet-item').removeClass('active');
                $('.slider-bullet-item:nth-child('+ ctr +')').addClass('active');
            }

        // End Image Slider

        // Sorter

        $('[data-sort]').click(function() {
            var s = $(this).attr('data-sort');
            if(s != 0) {
                $('tr[data-type != "'+ s +'"]').fadeOut();
                $('tr[data-type="'+ s +'"]').fadeIn();
            } else {
                $('tr[data-type]').fadeIn();
            }
        });


        // Tab-Pane
        $('a[data-activate-id]').click(function() {
            var id = $(this).attr('data-activate-id');
            var type = $(this).attr('data-tab-type');

            $(".tab-pane[id!='"+id+"'][data-tab-type='"+type+"']").fadeOut('fast');
            $("#"+ id +"").delay(200).fadeIn('slow');

            return false;
        });
    });
</script>

</html>
