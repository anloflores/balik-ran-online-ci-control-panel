<?php
    $this->load->view('inc/head_view');
    $this->load->view('inc/nav_view');
?>

    <style media="screen">
        .link {
            color: #ff5e00;
        }
    </style>

    <!-- Main Content -->
    <div class="container_12">
        <!-- Content -->
        <div class="grid_8">
            <div class="panel-container">
                <div class="panel-header">
                    <h1>Convert Game Time to <?=$this->config->item('vpoint_name')?></h1>
                </div>

                <div class="panel-content">


                    <form action="<?=base_url()?>user/gttovp/" method="post">
                        <div class="form-group">
                            <p>
                                <?=$this->config->item('gt_to_vp')?> Mins = 1 Vote Points
                            </p>

                            <center><p>
                                You Currently have  <span class="label bolder red"><?=$time_avail?></span> to receive
                            </p></center>

                            <?=$this->session->flashdata('gttovp_err')?>
                        </div>

                        <Br/>
                        <center>
                            <?php if ($time_avail != 0): ?>
                                <p>
                                    You don't have enough Game Time to convert.
                                </p>
                                <input type="submit" name="convert" value="Convert">
                            <?php endif; ?>
                        </center>
                    </form>
                </div>
            </div>
        </div>

        <!-- Side Bar -->
        <div class="grid_4">
            <!-- login -->
            <div class="grid_4">
                <!-- login -->
                <?php $this->load->view('mod/panel_login_view') ?>
                <?php $this->load->view('mod/panel_ranking_view') ?>
            </div>
        </div>
    </div>

    <?php $this->load->view('inc/footer_view') ?>

</body>
<script src="<?=base_url()?>assets/js/jquery-1.11.0.min.js" charset="utf-8"></script>
<script type="text/javascript">
    $(function() {
        // Tab-Pane
        $('a[data-activate-id]').click(function() {
            var id = $(this).attr('data-activate-id');
            var type = $(this).attr('data-tab-type');



            $(".tab-pane[id!='"+id+"'][data-tab-type='"+type+"']").fadeOut('fast');
            $("#"+ id +"").delay(200).fadeIn('slow');

            return false;
        });
    });
</script>
</html>
