<?php
    $this->load->view('inc/head_view');
    $this->load->view('inc/nav_view');
?>

    <style media="screen">
        .link {
            color: #ff5e00;
        }
    </style>

    <!-- Main Content -->
    <div class="container_12">
        <!-- Content -->
        <div class="grid_8">
            <div class="panel-container">
                <div class="panel-header">
                    <h1>Change Email Address</h1>
                </div>

                <div class="panel-content">


                    <form action="<?=base_url()?>user/changeemail/" method="post">
                        <div class="form-group">
                            <p>
                            Change Email Address requires your old and new Email Address
                            </p>

                            <?=$this->session->flashdata('cerr')?>

                            <div class="col_4">
                                <label for="username">Current Email Address</label>
                                <input id="username" name="old" class="full" type="text" placeholder="Current Email Address">
                            </div>
                        </div>

                        <div class="form-group">
                            <p>

                            </p>
                            <div class="col_2">
                                <label for="username">New Email Address</label>
                                <input id="username" name="new" class="full" type="text" placeholder="New Email Address">
                            </div>

                            <div class="col_2">
                                <label for="username">Re New Email Address</label>
                                <input id="username" name="newr" class="full" type="text" placeholder="Re Type New Email Address">
                            </div>
                        </div>
                        <Br/>
                        <center>
                            <input type="submit" value="Change Email">
                        </center>
                    </form>
                </div>
            </div>
        </div>

        <!-- Side Bar -->
        <div class="grid_4">
            <!-- login -->
            <div class="grid_4">
                <!-- login -->
                <?php $this->load->view('mod/panel_login_view') ?>
                <?php $this->load->view('mod/panel_ranking_view') ?>
            </div>
        </div>
    </div>

    <?php $this->load->view('inc/footer_view') ?>

</body>
<script src="<?=base_url()?>assets/js/jquery-1.11.0.min.js" charset="utf-8"></script>
<script type="text/javascript">
    $(function() {
        // Tab-Pane
        $('a[data-activate-id]').click(function() {
            var id = $(this).attr('data-activate-id');
            var type = $(this).attr('data-tab-type');



            $(".tab-pane[id!='"+id+"'][data-tab-type='"+type+"']").fadeOut('fast');
            $("#"+ id +"").delay(200).fadeIn('slow');

            return false;
        });
    });
</script>
</html>
