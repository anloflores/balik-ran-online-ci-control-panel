<?php
    $this->load->view('inc/head_view');
    $this->load->view('inc/nav_view');
?>

    <style media="screen">
        .link {
            color: #ff5e00;
        }

        table {
            border-collapse: separate;
            border-spacing: 0px 2px;
        }

        table tr th, td {
            text-align: center;
            padding: 5px;
        }

        table tr th {
            font: 700 12px 'Open Sans', sans-serif;
        }

        table tr {
            padding-bottom: 5px;
        }

        table tr:not(.table-head) {
            background: #ff5e00;
            color: #511e00;
            transition: all 0.5s ease;
        }

        table tr:not(.table-head):hover {
            background: #511e00;
            color: #fff;
        }

        .table-head {
            border-bottom: 1px solid #ff5e00;
        }
    </style>

    <!-- Main Content -->
    <div class="container_12">
        <!-- Content -->
        <div class="grid_8">
            <div class="panel-container">
                <div class="panel-header">
                    <h1>Characters</h1>
                </div>

                <div class="panel-content">
                    <h3>Strife Server</h3>
                    <table class="news-holder">
                        <?php $u = $this->User_Model->get_username($this->session->userdata['UserName']); ?>
                        <tr class="table-head">
                            <th>IGN</th>
                            <th>Lvl</th>
                            <th>Pow</th>
                            <th>Dex</th>
                            <th>Spi</th>
                            <th>Str</th>
                            <th>Strong</th>
                            <th>Gold</th>
                        </tr>
                        <?php foreach ($this->Game_Model->get_characters($u->UserNum) as $key => $var): ?>
                            <?php
                                $sc = "";
                                switch ($var->ChaSchool) {
                                    case 0: $sc = "SG";  break;
                                    case 1: $sc = "MP";  break;
                                    case 2: $sc = "PHX";  break;
                                    default: $sc = "NA"; break;
                                }
                             ?>
                            <tr>
                                <td><span class="label bolder blue" style="color: #fff;"><?=$sc?></span> <?=$var->ChaName?></td>
                                <td><?=$var->ChaLevel?></td>
                                <td><?=$var->ChaPower?></td>
                                <td><?=$var->ChaDex?></td>
                                <td><?=$var->ChaSpirit?></td>
                                <td><?=$var->ChaStrength?></td>
                                <td><?=$var->ChaStrong?></td>
                                <td><?=$var->ChaMoney?></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>

                    <h3>Havoc Server</h3>
                    <table class="news-holder">
                        <?php
                            $this->Game_Model->set_dbn("BalikGameHavoc");
                            $u = $this->User_Model->get_username($this->session->userdata['UserName']);
                        ?>
                        <tr class="table-head">
                            <th>IGN</th>
                            <th>Lvl</th>
                            <th>Pow</th>
                            <th>Dex</th>
                            <th>Spi</th>
                            <th>Str</th>
                            <th>Strong</th>
                            <th>Gold</th>
                        </tr>
                        <?php foreach ($this->Game_Model->get_characters($u->UserNum) as $key => $var): ?>
                            <tr>
                                <td><?=$var->ChaName?></td>
                                <td><?=$var->ChaLevel?></td>
                                <td><?=$var->ChaPower?></td>
                                <td><?=$var->ChaDex?></td>
                                <td><?=$var->ChaSpirit?></td>
                                <td><?=$var->ChaStrength?></td>
                                <td><?=$var->ChaStrong?></td>
                                <td><?=$var->ChaMoney?></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>

        <!-- Side Bar -->
        <div class="grid_4">
            <!-- login -->
            <div class="grid_4">
                <!-- login -->
                <?php $this->load->view('mod/panel_login_view') ?>
                <?php $this->load->view('mod/panel_ranking_view') ?>
            </div>
        </div>
    </div>

    <?php $this->load->view('inc/footer_view') ?>

</body>
<script src="<?=base_url()?>assets/js/jquery-1.11.0.min.js" charset="utf-8"></script>
<script type="text/javascript">
    $(function() {
        // Tab-Pane
        $('a[data-activate-id]').click(function() {
            var id = $(this).attr('data-activate-id');
            var type = $(this).attr('data-tab-type');



            $(".tab-pane[id!='"+id+"'][data-tab-type='"+type+"']").fadeOut('fast');
            $("#"+ id +"").delay(200).fadeIn('slow');

            return false;
        });
    });
</script>
</html>
