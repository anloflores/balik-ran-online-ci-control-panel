<?php
    $this->load->view('inc/head_view');
    $this->load->view('inc/nav_view');
?>

    <style media="screen">
        .link {
            color: #ff5e00;
        }
    </style>

    <!-- Main Content -->
    <div class="container_12">
        <!-- Content -->
        <div class="grid_8">
            <div class="panel-container">
                <div class="panel-header">
                    <h1>Change Pin Code</h1>
                </div>

                <div class="panel-content">


                    <form action="<?=base_url()?>user/changepincode/" method="post">
                        <div class="form-group">
                            <p>
                                Change Pin Code requires your current Pin Code and your new Pin Code.
                            </p>

                            <?=$this->session->flashdata('cpcerr')?>

                            <div class="col_4">
                                <label for="username">Current Pin Code</label>
                                <input id="username" name="old" class="full" type="password" placeholder="Current Pin Code">
                            </div>
                        </div>

                        <div class="form-group">
                            <p>

                            </p>
                            <div class="col_2">
                                <label for="username">New Pin Code</label>
                                <input id="username" name="new" class="full" type="password" placeholder="New Pin Code">
                            </div>

                            <div class="col_2">
                                <label for="username">Re New Pin Code</label>
                                <input id="username" name="newr" class="full" type="password" placeholder="Re Type New Pin Code">
                            </div>
                        </div>
                        <Br/>
                        <center>
                            <input type="submit" value="Change Pin Code">
                        </center>
                    </form>
                </div>
            </div>
        </div>

        <!-- Side Bar -->
        <div class="grid_4">
            <!-- login -->
            <div class="grid_4">
                <!-- login -->
                <?php $this->load->view('mod/panel_login_view') ?>
                <?php $this->load->view('mod/panel_ranking_view') ?>
            </div>
        </div>
    </div>

    <?php $this->load->view('inc/footer_view') ?>

</body>
<script src="<?=base_url()?>assets/js/jquery-1.11.0.min.js" charset="utf-8"></script>
<script type="text/javascript">
    $(function() {
        // Tab-Pane
        $('a[data-activate-id]').click(function() {
            var id = $(this).attr('data-activate-id');
            var type = $(this).attr('data-tab-type');



            $(".tab-pane[id!='"+id+"'][data-tab-type='"+type+"']").fadeOut('fast');
            $("#"+ id +"").delay(200).fadeIn('slow');

            return false;
        });
    });
</script>
</html>
