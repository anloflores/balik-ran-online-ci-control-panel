<?php
    $this->load->view('inc/head_view');
    $this->load->view('inc/nav_view');
?>

    <style media="screen">
        .link {
            color: #ff5e00;
        }

        table {
            width: 100%;
        }

        table thead {
            background: #e84700;
        }

        th {
            padding: 5px;
            font-weight: 600;
        }

        td {
            padding : 5px;
        }
    </style>

    <!-- Main Content -->
    <div class="container_12">
        <!-- Content -->
        <div class="grid_8">
            <div class="panel-container">
                <div class="panel-header">
                    <h1>TopU Records</h1>
                </div>

                <div class="panel-content">
                        <table class="rankings-tbl"  style="width: 100%;">
                            <thead>
                                <tr>
                                    <td>Code</td>
                                    <td>Pin</td>
                                    <td>Amount</td>
                                </tr>
                            </thead>

                            <tbody>
                                <?php foreach ($this->CP_Model->get_record('.TopupRecord', $this->session->userdata('UserName')) as $key => $var): ?>
                                    <tr>
                                        <td><?=$var->Code?></td>
                                        <td><?=$var->Pin?></td>
                                        <td><?=$var->Amount?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                </div>
            </div>
        </div>

        <!-- Side Bar -->
        <div class="grid_4">
            <!-- login -->
            <div class="grid_4">
                <!-- login -->
                <?php $this->load->view('mod/panel_login_view') ?>
                <?php $this->load->view('mod/panel_ranking_view') ?>
            </div>
        </div>
    </div>

    <?php $this->load->view('inc/footer_view') ?>

</body>
<script src="<?=base_url()?>assets/js/jquery-1.11.0.min.js" charset="utf-8"></script>
<script type="text/javascript">
    $(function() {
        // Tab-Pane
        $('a[data-activate-id]').click(function() {
            var id = $(this).attr('data-activate-id');
            var type = $(this).attr('data-tab-type');



            $(".tab-pane[id!='"+id+"'][data-tab-type='"+type+"']").fadeOut('fast');
            $("#"+ id +"").delay(200).fadeIn('slow');

            return false;
        });
    });
</script>
</html>
