<?php
    $this->load->view('inc/head_view');
    $this->load->view('inc/nav_view');
?>

    <style media="screen">
        .link {
            color: #ff5e00;
        }

        table {
            border-collapse: separate;
            border-spacing: 0px 2px;
            width: 100%;
        }

        table tr th, td {
            text-align: center;
            padding: 5px;
        }

        table tr th {
            font: 700 12px 'Open Sans', sans-serif;
        }

        table tr {
            padding-bottom: 5px;
        }

        table tr:not(.table-head) {
            background: #ff5e00;
            color: #511e00;
            transition: all 0.5s ease;
        }

        table tr:not(.table-head):hover {
            background: #511e00;
            color: #fff;
        }

        .table-head {
            border-bottom: 1px solid #ff5e00;
        }

        input[type="radio"] {
            background: transparent;
            outline: none !important;
            border: none;
        }
    </style>

    <!-- Main Content -->
    <div class="container_12">
        <!-- Content -->
        <div class="grid_8">
            <div class="panel-container">
                <div class="panel-header">
                    <h1>Change School</h1>
                </div>

                <div class="panel-content">
                    <form class="" action="<?=base_url()?>user/changeschool/<?=$school?>/" method="post">
                        <div class="form-group">
                            <div class="push_2 grid_4">
                                <center>
                                    <center>
                                        <span class="option">
                                            <a href="<?=base_url()?>user/changeschool/">
                                                Strife
                                            </a>
                                        </span>
                                        <span class="option separate">/</span>
                                        <span class="option">
                                            <a href="<?=base_url()?>user/changeschool/havoc/">
                                                Havok
                                            </a>
                                        </span>
                                    </center>
                                    
                                    <h3><?=$server?> Server</h3>
                                    <p>
                                        You need to have 400 VP to be able to Change School.
                                    </p>
                                </center>
                                <?=$this->session->flashdata('cs_err')?>
                                <table class="news-holder">
                                    <?php $u = $this->User_Model->get_username($this->session->userdata['UserName']); ?>
                                    <tr class="table-head">
                                        <th></th>
                                        <th>IGN</th>
                                        <th>Lvl</th>
                                    </tr>
                                    <?php foreach ($this->Game_Model->get_characters($u->UserNum) as $key => $var): ?>
                                        <?php
                                            $sc = "";
                                            switch ($var->ChaSchool) {
                                                case 0: $sc = "SG";  break;
                                                case 1: $sc = "MP";  break;
                                                case 2: $sc = "PHX";  break;
                                                default: $sc = "NA"; break;
                                            }
                                         ?>
                                        <tr>
                                            <td><input type="radio" name="char" data-sc="<?=$var->ChaSchool?>" value="<?=$var->ChaNum?>"></td>
                                            <td style="text-align: left !important;"><span class="label bolder blue" style="color: #fff;"><?=$sc?></span> <?= $var->ChaName?></td>
                                            <td><?=$var->ChaLevel?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>

                                <label for="school">Select School</label>
                                <select name="school" id="school" class="full">
                                    <option value="-1">Select School</option>
                                    <option value="0">Sacred Gate</option>
                                    <option value="1">Mystic Peak</option>
                                    <option value="2">Phoenix</option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                            <Br/>
                            <center>
                                <input type="submit" value="Change School">
                            </center>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Side Bar -->
        <div class="grid_4">
            <!-- login -->
            <div class="grid_4">
                <!-- login -->
                <?php $this->load->view('mod/panel_login_view') ?>
                <?php $this->load->view('mod/panel_ranking_view') ?>
            </div>
        </div>
    </div>

    <?php $this->load->view('inc/footer_view') ?>

</body>
<script src="<?=base_url()?>assets/js/jquery-1.11.0.min.js" charset="utf-8"></script>
<script type="text/javascript">
    $(function() {

        $('input[type="radio"]').on('change', function() {
            var v = $(this).attr('data-sc');
            $("#school").val('-1');
            $("#school").find('option').show();
            $("#school option[value='"+ v +"']").hide();
        });

        // Tab-Pane
        $('a[data-activate-id]').click(function() {
            var id = $(this).attr('data-activate-id');
            var type = $(this).attr('data-tab-type');



            $(".tab-pane[id!='"+id+"'][data-tab-type='"+type+"']").fadeOut('fast');
            $("#"+ id +"").delay(200).fadeIn('slow');

            return false;
        });
    });
</script>
</html>
