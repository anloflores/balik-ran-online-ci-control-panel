<?php
    $this->load->view('inc/head_view');
    $this->load->view('inc/nav_view');
?>


    <!-- Main Content -->
    <div class="container_12">
        <!-- Content -->
        <div class="push_4 grid_4">
            <?php $this->load->view('mod/panel_login_view') ?>
        </div>
    </div>

    <?php $this->load->view('inc/footer_view') ?>

</body>
<script src="<?=base_url()?>assets/js/jquery-1.11.0.min.js" charset="utf-8"></script>
<script type="text/javascript">
    $(function() {
        // Image Slider
            var _slider = $('.image-slider ul');
            var _count = $('li[data-image]').length;
            var _ctr = 1;
            var _rotateRate = 3000;

            var rot = setInterval(function() {
                if(_ctr != _count) {
                      $('li[data-image]:nth-child('+ _ctr +')').fadeOut("slow");
                      _ctr++;
                      $('li[data-image]:nth-child('+ _ctr +')').fadeIn("slow");

                      activate_bullet(_ctr);
                } else {
                      $('li[data-image]:nth-child(1)').fadeIn("slow");
                      $('li[data-image]:nth-child('+ _count +')').fadeOut("slow");
                      _ctr = 1;

                      activate_bullet(_ctr);
                }
            }, _rotateRate);

            $("li[data-image]").each(function(k, v) {
                var ch = "";

                if(k == 0)
                    ch = "active";

                var bg = $(this).attr('data-image');
                $(this).css('background-image', 'url('+ bg +')');
                $('ul.slider-bullet').append('<li class="slider-bullet-item '+ ch +'"></li>');
            });

            $(document).on('click', '.slider-bullet-item', function() {
                var i = $(this).index()+1;
                $('li[data-image]:nth-child('+ i +')').fadeIn("slow");
                $('li[data-image]:nth-child('+ _ctr +')').fadeOut("fast");
                _ctr = i;
                activate_bullet(_ctr);
            });
            function activate_bullet(ctr) {
                $('.slider-bullet-item').removeClass('active');
                $('.slider-bullet-item:nth-child('+ ctr +')').addClass('active');
            }

        // End Image Slider


        // Tab-Pane
        $('a[data-activate-id]').click(function() {
            var id = $(this).attr('data-activate-id');
            var type = $(this).attr('data-tab-type');



            $(".tab-pane[id!='"+id+"'][data-tab-type='"+type+"']").fadeOut('fast');
            $("#"+ id +"").delay(200).fadeIn('slow');

            return false;
        });
    });
</script>

</html>
