<div class="panel-container">
    <div class="panel-header">
        <h1><?=($this->session->userdata('UserName') === null) ? "Login" : "Account"?></h1>
    </div>

    <div class="panel-content">
        <?php if ($this->session->userdata('UserName') === null): ?>
        <center>
            <?=$this->session->flashdata('log_error')?>
        </center>

        <form class="login-form" action="<?=base_url()?>home/login/" method="post">
            <div class="grid_2">
                <input class="login-field" type="text" name="username" placeholder="Username">
                <input class="login-field" type="password" name="password" placeholder="Password">
                <input type="hidden" name="url" value="<?=current_url()?>">
            </div>

            <div class="grid_1">
                <input type="submit" name="user_login" value="Login">
            </div>

            <div class="clearfix"></div>
        </form>
        <br/>
        <center>
            <span class="option">
                <a href="<?=base_url()?>home/register">
                    Register
                </a>
            </span>
            <span class="option separate">/</span>
            <span class="option">
                <a href="<?=base_url()?>home/forgotpw/">
                    Forgot Password?
                </a>
            </span>
        </center>


        <?php else: ?>
            <center><?=$this->session->flashdata('reg_err')?></center>
            <!-- LOGGED IN! -->
            <?php $username = $this->session->userdata('UserName') ?>
            Welcome, <?=$username?>!<Br/>
            <small><?=$this->User_Model->get_username($username)->UserEmail?></small><BR/>
            <span class="label bolder red"><?=$this->config->item('point_name')?> : <?=$this->CP_Model->get_points($username) !== null ? $this->CP_Model->get_points($username)->Points : "0"?></span>
            <span class="label bolder blue"><?=$this->config->item('vpoint_name')?> : <?=$this->CP_Model->get_points($username) !== null ? $this->CP_Model->get_points($username)->VPoints : "0"?></span>
            <div class="clearfix"></div><BR/>
            <a href="<?=base_url()?>home/logout/"><span class="label red">Logout</span></a>
            <div class="clearfix"></div><br/>

            <ul class="User-Menu" style="margin-bottom: 0;"><li style="list-style: none;">Account Management</li></ul>
            <ul class="User-Menu">
                <li><a href="<?=base_url()?>user/topup/">Topup</a></li>
                <li><a href="<?=base_url()?>user/changepassword/">Change Password</a></li>
                <li><a href="<?=base_url()?>user/changepincode/">Change Pin Code</a></li>
                <li><a href="<?=base_url()?>user/changeemail/">Change Email Address</a></li>
            </ul>

            <ul class="User-Menu" style="margin-bottom: 0;"><li style="list-style: none;">Character Management</li></ul>
            <ul class="User-Menu">
                <li><a href="<?=base_url()?>user/changeschool/">Change School</a></li>
                <li><a href="<?=base_url()?>user/chadisplay/">Character Display</a></li>
            </ul>

            <ul class="User-Menu" style="margin-bottom: 0;"><li style="list-style: none;">Point Conversion</li></ul>
            <ul class="User-Menu">
                <li><a href="<?=base_url()?>user/eptvp/">E-Points to Vote Points</a></li>
                <li><a href="<?=base_url()?>user/gttovp/">Game Time to Vote Points</a></li>
            </ul>

            <ul class="User-Menu" style="margin-bottom: 0;"><li style="list-style: none;">Records</li></ul>
            <ul class="User-Menu">
                <li><a href="<?=base_url()?>user/isrecord/">Item Shop Records</a></li>
                <li><a href="<?=base_url()?>user/trecord/">Topup Record</a></li>
                <li><a href="<?=base_url()?>user/crecord/">Convert Record</a></li>
            </ul>

        <?php endif; ?>
    </div>
</div>
