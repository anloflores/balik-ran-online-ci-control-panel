<div class="panel-container">
    <div class="panel-header">
        <h1>Rankings</h1>
    </div>

    <div class="panel-content">
        <center>
            <span class="option">
                <a href="#" data-activate-id="strife" data-tab-type="server">
                    Strife
                </a>
            </span>
            <span class="option separate">/</span>
            <span class="option">
                <a href="#" data-activate-id="havok" data-tab-type="server">
                    Havoc
                </a>
            </span>
        </center>

        <div class="tab-holder rank-container">
            <!-- Class Topnothers -->
            <div id="strife" class="tab-pane" data-active="true" data-tab-type="server">
                <center>
                    <span class="option">
                        <a href="#" data-activate-id="classtopnothers" data-tab-type="ranking">
                            Class Topnothers
                        </a>
                    </span>
                    <span class="option separate">/</span>
                    <span class="option">
                        <a href="#" data-activate-id="clubwar" data-tab-type="ranking">
                            Club War
                        </a>
                    </span>
                </center>

                <div class="tab-holder rank-container">
                    <!-- Class Topnothers -->
                    <div id="classtopnothers" class="tab-pane" data-active="true" data-tab-type="ranking">
                        <?php if ($this->User_Model->get_ranking_top(1, 1, array(1,64)) !== null): ?>
                            <?php $var = $this->User_Model->get_ranking_top(1, 1, array(1,64))?>
                            <div class="rank-item">
                                <br/>
                                <div class="grid_1">
                                    <img class="ranking-thumb" src="<?=base_url()?>assets/img/class/<?=$var->ChaClass?>.jpg" alt="">
                                </div>

                                <div class="grid_2">
                                    <div class="ranking-name">
                                        <?php if ($var->ChaOnline): ?>
                                            <i class="fa fa-check" style="color: green;"></i>
                                        <?php else: ?>
                                            <i class="fa fa-close" style="color: red;"></i>
                                        <?php endif; ?>
                                        <?=$var->ChaName?>
                                    </div>
                                    <div class="label-holder">
                                        <span class="label red">LVL: <?=$var->ChaLevel?></span>
                                        <span class="label orange">K/D: <?=($var->SWKill+$var->CWKill+$var->RhdmKill)?>/<?=($var->SWDeath+$var->CWDeath+$var->RhdmDeath)?></span>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        <?php endif; ?>

                        <?php if ($this->User_Model->get_ranking_top(1, 1, array(2,128)) !== null): ?>
                            <?php $var = $this->User_Model->get_ranking_top(1, 1, array(2,128))?>
                            <div class="rank-item">
                                <br/>
                                <div class="grid_1">
                                    <img class="ranking-thumb" src="<?=base_url()?>assets/img/class/<?=$var->ChaClass?>.jpg" alt="">
                                </div>

                                <div class="grid_2">
                                    <div class="ranking-name">
                                        <?php if ($var->ChaOnline): ?>
                                            <i class="fa fa-check" style="color: green;"></i>
                                        <?php else: ?>
                                            <i class="fa fa-close" style="color: red;"></i>
                                        <?php endif; ?>
                                        <?=$var->ChaName?>
                                    </div>
                                    <div class="label-holder">
                                        <span class="label red">LVL: <?=$var->ChaLevel?></span>
                                        <span class="label orange">K/D: <?=($var->SWKill+$var->CWKill+$var->RhdmKill)?>/<?=($var->SWDeath+$var->CWDeath+$var->RhdmDeath)?></span>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        <?php endif; ?>

                        <?php if ($this->User_Model->get_ranking_top(1, 1, array(4,256)) !== null): ?>
                            <?php $var = $this->User_Model->get_ranking_top(1, 1, array(4,256))?>
                            <div class="rank-item">
                                <br/>
                                <div class="grid_1">
                                    <img class="ranking-thumb" src="<?=base_url()?>assets/img/class/<?=$var->ChaClass?>.jpg" alt="">
                                </div>

                                <div class="grid_2">
                                    <div class="ranking-name">
                                        <?php if ($var->ChaOnline): ?>
                                            <i class="fa fa-check" style="color: green;"></i>
                                        <?php else: ?>
                                            <i class="fa fa-close" style="color: red;"></i>
                                        <?php endif; ?>
                                        <?=$var->ChaName?>
                                    </div>
                                    <div class="label-holder">
                                        <span class="label red">LVL: <?=$var->ChaLevel?></span>
                                        <span class="label orange">K/D: <?=($var->SWKill+$var->CWKill+$var->RhdmKill)?>/<?=($var->SWDeath+$var->CWDeath+$var->RhdmDeath)?></span>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        <?php endif; ?>

                        <?php if ($this->User_Model->get_ranking_top(1, 1, array(8, 512)) !== null): ?>
                            <?php $var = $this->User_Model->get_ranking_top(1, 1, array(8, 512))?>
                            <div class="rank-item">
                                <br/>
                                <div class="grid_1">
                                    <img class="ranking-thumb" src="<?=base_url()?>assets/img/class/<?=$var->ChaClass?>.jpg" alt="">
                                </div>

                                <div class="grid_2">
                                    <div class="ranking-name">
                                        <?php if ($var->ChaOnline): ?>
                                            <i class="fa fa-check" style="color: green;"></i>
                                        <?php else: ?>
                                            <i class="fa fa-close" style="color: red;"></i>
                                        <?php endif; ?>
                                        <?=$var->ChaName?>
                                    </div>
                                    <div class="label-holder">
                                        <span class="label red">LVL: <?=$var->ChaLevel?></span>
                                        <span class="label orange">K/D: <?=($var->SWKill+$var->CWKill+$var->RhdmKill)?>/<?=($var->SWDeath+$var->CWDeath+$var->RhdmDeath)?></span>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        <?php endif; ?>
                    </div>

                    <!-- Clubwar -->
                    <div id="clubwar" class="tab-pane" data-tab-type="ranking">
                        <?php foreach ($this->Game_Model->club_rank() as $key => $var): ?>
                            <div class="rank-item">
                                <br/>
                                <div class="grid_1">
                                    <img class="ranking-thumb" src="<?=base_url()?>assets/img/school/<?=$var->RegionID?>.png" alt="">
                                </div>

                                <div class="grid_2">
                                    <div class="ranking-name"><?=$var->GuName?></div>
                                    <div class="label-holder">
                                        <span class="label red">CL : <?=$this->Game_Model->ChaInfo($var->ChaNum)->ChaName?></span>
                                        <div class="clearfix"></div>
                                        <span class="label orange">Mem # : <?=count($this->Game_Model->club_member($var->GuNum))?></span>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        <?php endforeach; ?>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>

            <!-- Clubwar -->
            <div id="havok" class="tab-pane" data-tab-type="server">
                <center>
                    <span class="option">
                        <a href="#" data-activate-id="classtopnothers" data-tab-type="ranking">
                            Class Topnothers
                        </a>
                    </span>
                    <span class="option separate">/</span>
                    <span class="option">
                        <a href="#" data-activate-id="clubwar" data-tab-type="ranking">
                            Club War
                        </a>
                    </span>
                </center>

                <div class="tab-holder rank-container">
                    <!-- Class Topnothers -->
                    <div id="classtopnothers" class="tab-pane" data-active="true" data-tab-type="ranking">
                        <?php if ($this->User_Model->get_ranking_top(2, 1, array(1,64)) !== null): ?>
                            <?php $var = $this->User_Model->get_ranking_top(2, 1, array(1,64))?>
                            <div class="rank-item">
                                <br/>
                                <div class="grid_1">
                                    <img class="ranking-thumb" src="<?=base_url()?>assets/img/class/<?=$var->ChaClass?>.jpg" alt="">
                                </div>

                                <div class="grid_2">
                                    <div class="ranking-name">
                                        <?php if ($var->ChaOnline): ?>
                                            <i class="fa fa-check" style="color: green;"></i>
                                        <?php else: ?>
                                            <i class="fa fa-close" style="color: red;"></i>
                                        <?php endif; ?>
                                        <?=$var->ChaName?>
                                    </div>
                                    <div class="label-holder">
                                        <span class="label red">LVL: <?=$var->ChaLevel?></span>
                                        <span class="label orange">K/D: <?=($var->SWKill+$var->CWKill+$var->RhdmKill)?>/<?=($var->SWDeath+$var->CWDeath+$var->RhdmDeath)?></span>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        <?php endif; ?>

                        <?php if ($this->User_Model->get_ranking_top(2, 1, array(2,128)) !== null): ?>
                            <?php $var = $this->User_Model->get_ranking_top(2, 1, array(2,128))?>
                            <div class="rank-item">
                                <br/>
                                <div class="grid_1">
                                    <img class="ranking-thumb" src="<?=base_url()?>assets/img/class/<?=$var->ChaClass?>.jpg" alt="">
                                </div>

                                <div class="grid_2">
                                    <div class="ranking-name">
                                        <?php if ($var->ChaOnline): ?>
                                            <i class="fa fa-check" style="color: green;"></i>
                                        <?php else: ?>
                                            <i class="fa fa-close" style="color: red;"></i>
                                        <?php endif; ?>
                                        <?=$var->ChaName?>
                                    </div>
                                    <div class="label-holder">
                                        <span class="label red">LVL: <?=$var->ChaLevel?></span>
                                        <span class="label orange">K/D: <?=($var->SWKill+$var->CWKill+$var->RhdmKill)?>/<?=($var->SWDeath+$var->CWDeath+$var->RhdmDeath)?></span>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        <?php endif; ?>

                        <?php if ($this->User_Model->get_ranking_top(2, 1, array(4,256)) !== null): ?>
                            <?php $var = $this->User_Model->get_ranking_top(2, 1, array(4,256))?>
                            <div class="rank-item">
                                <br/>
                                <div class="grid_1">
                                    <img class="ranking-thumb" src="<?=base_url()?>assets/img/class/<?=$var->ChaClass?>.jpg" alt="">
                                </div>

                                <div class="grid_2">
                                    <div class="ranking-name">
                                        <?php if ($var->ChaOnline): ?>
                                            <i class="fa fa-check" style="color: green;"></i>
                                        <?php else: ?>
                                            <i class="fa fa-close" style="color: red;"></i>
                                        <?php endif; ?>
                                        <?=$var->ChaName?>
                                    </div>
                                    <div class="label-holder">
                                        <span class="label red">LVL: <?=$var->ChaLevel?></span>
                                        <span class="label orange">K/D: <?=($var->SWKill+$var->CWKill+$var->RhdmKill)?>/<?=($var->SWDeath+$var->CWDeath+$var->RhdmDeath)?></span>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        <?php endif; ?>

                        <?php if ($this->User_Model->get_ranking_top(2, 1, array(8, 512)) !== null): ?>
                            <?php $var = $this->User_Model->get_ranking_top(2, 1, array(8, 512))?>
                            <div class="rank-item">
                                <br/>
                                <div class="grid_1">
                                    <img class="ranking-thumb" src="<?=base_url()?>assets/img/class/<?=$var->ChaClass?>.jpg" alt="">
                                </div>

                                <div class="grid_2">
                                    <div class="ranking-name">
                                        <?php if ($var->ChaOnline): ?>
                                            <i class="fa fa-check" style="color: green;"></i>
                                        <?php else: ?>
                                            <i class="fa fa-close" style="color: red;"></i>
                                        <?php endif; ?>
                                        <?=$var->ChaName?>
                                    </div>
                                    <div class="label-holder">
                                        <span class="label red">LVL: <?=$var->ChaLevel?></span>
                                        <span class="label orange">K/D: <?=($var->SWKill+$var->CWKill+$var->RhdmKill)?>/<?=($var->SWDeath+$var->CWDeath+$var->RhdmDeath)?></span>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        <?php endif; ?>
                    </div>

                    <!-- Clubwar -->
                    <div id="clubwar" class="tab-pane" data-tab-type="ranking">
                        <?php $this->Game_Model->set_dbn('BalikGameHavoc')?>
                        <?php foreach ($this->Game_Model->club_rank() as $key => $var): ?>
                            <div class="rank-item">
                                <br/>
                                <div class="grid_1">
                                    <img class="ranking-thumb" src="<?=base_url()?>assets/img/school/<?=$var->RegionID?>.png" alt="">
                                </div>

                                <div class="grid_2">
                                    <div class="ranking-name"><?=$var->GuName?></div>
                                    <div class="label-holder">
                                        <span class="label red">CL : <?=$this->Game_Model->ChaInfo($var->ChaNum)->ChaName?></span>
                                        <div class="clearfix"></div>
                                        <span class="label orange">Mem # : <?=count($this->Game_Model->club_member($var->GuNum))?></span>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        <?php endforeach; ?>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
