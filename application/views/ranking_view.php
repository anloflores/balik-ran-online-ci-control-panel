<?php
    $this->load->view('inc/head_view');
    $this->load->view('inc/nav_view');
?>


    <!-- Main Content -->
    <div class="container_12">
        <!-- Content -->
        <div class="grid_8">
            <div class="panel-container">
                <div class="panel-header">
                    <h1>Rankings</h1>
                </div>

                <div class="panel-content">
                    <center>
                        <small>Server</small><br/>
                        <span class="option">
                            <a href="<?=base_url()?>home/rankings/lvl/1/">
                                Strife
                            </a>
                        </span>
                        <span class="option separate">/</span>
                        <span class="option">
                            <a href="<?=base_url()?>home/rankings/lvl/2/">
                                Havoc
                            </a>
                        </span>
                    </center>
                    <center>
                        <small>CLASS</small><br/>
                        <span class="option">
                            <a href="<?=base_url()?>home/rankings/top/<?=$server?>/brawler/">
                                Brawler
                            </a>
                        </span>
                        <span class="option separate">/</span>
                        <span class="option">
                            <a href="<?=base_url()?>home/rankings/top/<?=$server?>/swordsman/">
                                Swordsman
                            </a>
                        </span>
                        <span class="option separate">/</span>
                        <span class="option">
                            <a href="<?=base_url()?>home/rankings/top/<?=$server?>/archer/">
                                Archer
                            </a>
                        </span>
                        <span class="option separate">/</span>
                        <span class="option">
                            <a href="<?=base_url()?>home/rankings/top/<?=$server?>/shamman/">
                                Shamman
                            </a>
                        </span>
                    </center>
                    <center>
                    <small>CATEGORIES</small><br/>
                        <span class="option">
                            <a href="<?=base_url()?>home/rankings/lvl/<?=$server?>/<?=$class?>">
                                Level
                            </a>
                        </span>
                        <span class="option separate">/</span>
                        <span class="option">
                            <a href="<?=base_url()?>home/rankings/cw/<?=$server?>/<?=$class?>">
                                Club Wars
                            </a>
                        </span>
                        <span class="option separate">/</span>
                        <span class="option">
                            <a href="<?=base_url()?>home/rankings/sw/<?=$server?>/<?=$class?>">
                                School Wars
                            </a>
                        </span>
                        <span class="option separate">/</span>
                        <span class="option">
                            <a href="<?=base_url()?>home/rankings/lvl/<?=$server?>/<?=$class?>">
                                Seige & Destroy
                            </a>
                        </span>
                        <span class="option separate">/</span>
                        <span class="option">
                            <a href="<?=base_url()?>home/rankings/pk/<?=$server?>/<?=$class?>">
                                PK
                            </a>
                        </span>
                        <span class="option separate">/</span>
                        <span class="option">
                            <a href="<?=base_url()?>home/rankings/gd/<?=$server?>/<?=$class?>">
                                Wealth
                            </a>
                        </span>
                    </center>
                    <table class="rankings-tbl">
                        <thead>
                           <th>#</th>
                           <th>Class</th>
                           <th>Name</th>
                           <th>Level</th>
                           <th>K/D</th>
                           <th>Wealth</th>
                           <th>Guild</th>
                       </thead>

                        <?php foreach ($players as $key => $var): ?>
                            <tr>
                                <td><span class="label blue"><?=$key+1?></span></td>
                                <td><img class="ranking-thumb" src="<?=base_url()?>assets/img/class/<?=$var->ChaClass?>.jpg" alt=""></td>
                                <td class="ranking-name"><?=$var->ChaName?></div></td>
                                <td><?=$var->ChaLevel?></td>
                                <td><?=($var->SWKill+$var->CWKill+$var->RhdmKill)?> / <?=($var->SWDeath+$var->CWDeath+$var->RhdmDeath)?></td>
                                <td><?=$var->ChaMoney?></td>
                                <td><?=$var->GuNum == 0? '-' : $this->Game_Model->get_guild($var->GuNum)->GuName?></td>
                            </tr>

                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>

        <!-- Side Bar -->
        <div class="grid_4">
            <!-- login -->
            <?php $this->load->view('mod/panel_login_view') ?>
            <?php $this->load->view('mod/panel_ranking_view') ?>
        </div>
    </div>

    <?php $this->load->view('inc/footer_view') ?>

</body>
<script src="<?=base_url()?>assets/js/jquery-1.11.0.min.js" charset="utf-8"></script>
<script type="text/javascript">
    $(function() {
        // Image Slider
            var _slider = $('.image-slider ul');
            var _count = $('li[data-image]').length;
            var _ctr = 1;
            var _rotateRate = 3000;

            var rot = setInterval(function() {
                if(_ctr != _count) {
                      $('li[data-image]:nth-child('+ _ctr +')').fadeOut("slow");
                      _ctr++;
                      $('li[data-image]:nth-child('+ _ctr +')').fadeIn("slow");

                      activate_bullet(_ctr);
                } else {
                      $('li[data-image]:nth-child(1)').fadeIn("slow");
                      $('li[data-image]:nth-child('+ _count +')').fadeOut("slow");
                      _ctr = 1;

                      activate_bullet(_ctr);
                }
            }, _rotateRate);

            $("li[data-image]").each(function(k, v) {
                var ch = "";

                if(k == 0)
                    ch = "active";

                var bg = $(this).attr('data-image');
                $(this).css('background-image', 'url('+ bg +')');
                $('ul.slider-bullet').append('<li class="slider-bullet-item '+ ch +'"></li>');
            });

            $(document).on('click', '.slider-bullet-item', function() {
                var i = $(this).index()+1;
                $('li[data-image]:nth-child('+ i +')').fadeIn("slow");
                $('li[data-image]:nth-child('+ _ctr +')').fadeOut("fast");
                _ctr = i;
                activate_bullet(_ctr);
            });
            function activate_bullet(ctr) {
                $('.slider-bullet-item').removeClass('active');
                $('.slider-bullet-item:nth-child('+ ctr +')').addClass('active');
            }

        // End Image Slider


        // Tab-Pane
        $('a[data-activate-id]').click(function() {
            var id = $(this).attr('data-activate-id');
            var type = $(this).attr('data-tab-type');



            $(".tab-pane[id!='"+id+"'][data-tab-type='"+type+"']").fadeOut('fast');
            $("#"+ id +"").delay(200).fadeIn('slow');

            return false;
        });
    });
</script>

</html>
