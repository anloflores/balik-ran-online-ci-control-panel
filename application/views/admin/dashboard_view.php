<?php $this->load->view('admin/inc/head_view') ?>
<?php $this->load->view('admin/inc/topbar_view') ?>
<?php $this->load->view('admin/inc/nav_view') ?>


        <div class="nine columns">
            <div class="row main-content">
                <div class="eight columns">
                    <div class="panel">
                        <div class="panel-header">
                            <h3 class="panel-title">Visitors Stat</h3>
                            <p class="panel-sub">Visitors Informations</p>
                        </div>

                        <div class="panel-content">
                            <canvas id="myChart4" style="width: 100%; height: 100%;"></canvas>
                        </div>
                    </div>
                </div>

                <div class="four columns">
                    <?php $this->load->view('admin/modules/panel_activities_view')?>
                </div>
            </div>
        </div>
    </body>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="<?=base_url()?>res/js/cs.common.js" charset="utf-8"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.min.js" charset="utf-8"></script>
    <script src="<?=base_url()?>res/js/cs.demo.chart.js" charset="utf-8"></script>
    <script src="<?=base_url()?>res/js/libs/gumby.min.js" charset="utf-8"></script>
</html>
