<?php $this->load->view('admin/inc/head_view') ?>
<?php $this->load->view('admin/inc/topbar_view') ?>
<?php $this->load->view('admin/inc/nav_view') ?>


        <div class="nine columns">
            <div class="row main-content">
                <div class="centered six columns">
                    <div class="panel">
                        <div class="panel-header">
                            <h3 class="panel-title">Create User<h3>
                            <p class="panel-sub">Create User to Access Admin Panel.</p>
                        </div>

                        <div class="panel-content">
                            <div class="row">
                                <div class="twelve columns">
                                    <?php if ($this->session->flashdata('err') !== null): ?>
                                        <div class="<?=$this->session->flashdata('err')['status'] == 0 ? 'danger' : 'success'?> alert" style="font-size: 10px !important;">
                                            <?=$this->session->flashdata('err')['error']?>
                                        </div>
                                    <?php endif; ?>

                                    <form class="field" action="<?=base_url()?>admin/user/create/" method="post">
                                        <div class="row">
                                            <small>USERNAME</small>
                                            <input type="text" class="input" name="username" value="<?=set_value('username')?>" placeholder="Username">
                                        </div>

                                        <div class="row">
                                            <div class="six columns">
                                                <small>FIRST NAME</small>
                                                <input type="text" class="input" name="fname" value="<?=set_value('fname')?>" placeholder="First Name">
                                            </div>
                                            <div class="six columns">
                                                <small>LAST NAME</small>
                                                <input type="text" class="input" name="lname" value="<?=set_value('lname')?>" placeholder="Last Name">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <small>E-MAIL</small>
                                            <input type="text" class="input" name="email" value="<?=set_value('email')?>" placeholder="E-mail">
                                        </div>

                                        <div class="row">
                                            <div class="six columns">
                                                <small>NEW PASSWORD</small>
                                                <input type="password" class="input" name="pass" placeholder="Password">
                                            </div>
                                            <div class="six columns">
                                                <small>RE NEW PASSWORD</small>
                                                <input type="password" class="input" name="rpass" placeholder="Re Password">
                                            </div>
                                        </div>

                                        <center>
                                            <input type="submit" value="Create Info" class="medium success btn">
                                        </center>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>


    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" media="screen" title="no title" charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="<?=base_url()?>res/js/cs.common.js" charset="utf-8"></script>
    <script src="<?=base_url()?>res/js/libs/gumby.min.js" charset="utf-8"></script>

</html>
