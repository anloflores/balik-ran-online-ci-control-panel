<?php $this->load->view('admin/inc/head_view') ?>
<?php $this->load->view('admin/inc/topbar_view') ?>
<?php $this->load->view('admin/inc/nav_view') ?>


        <div class="nine columns">
            <div class="row main-content">
                <div class="twelve columns">
                    <div class="panel">
                        <div class="panel-header">
                            <h3 class="panel-title">Edit Item #<?=$p->ProductNum?><h3>
                            <p class="panel-sub">Edit Item currenly in Item Shop.</p>
                        </div>

                        <div class="panel-content">
                            <div class="row">
                                <div class="centered eight columns">
                                    <?php if ($this->session->flashdata('err') !== null): ?>
                                        <div class="<?=$this->session->flashdata('err')['status'] == 0 ? 'danger' : 'success'?> alert" style="font-size: 10px !important;">
                                            <?=$this->session->flashdata('err')['error']?>
                                        </div>
                                    <?php endif; ?>

                                    <form class="field" action="<?=base_url()?>admin/item/edit/<?=$p->ProductNum?>/" method="post" enctype="multipart/form-data">
                                        <div class="row">
                                            <small>ITEM NAME</small>
                                            <input type="text" class="input" name="name" placeholder="Item Name" value="<?=$p->ItemName?>">
                                        </div>
                                        <div class="row">
                                            <div class="six columns"><small>ITEM MAIN</small><input type="text" value="<?=$p->ItemMain?>" class="input" name="main" placeholder="Item Main"></div>
                                            <div class="six columns"><small>ITEM SUB</small><input type="text" value="<?=$p->ItemSub?>" class="input" name="sub" placeholder="Item Sub"></div>
                                        </div>
                                        <div class="row">
                                            <div class="six columns"><small>ITEM PRICE</small><input type="number" value="<?=$p->ItemPrice?>" min="0" class="input" name="price" placeholder="Item Price"></div>
                                            <div class="six columns picker" style="margin-left: 13px">
                                                <small>ITEM SECTION</small>
                                                <select name="sec">
                                                    <option <?=$p->ItemSec == 1 ? 'selected' : ''?> value="1">EP Shop</option>
                                                    <option <?=$p->ItemSec == 2 ? 'selected' : ''?> value="2">VP Shop</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="six columns"><small>ITEM STOCK</small><input type="number" value="<?=$p->Itemstock?>" class="input" name="stock" placeholder="Item Stock"></div>
                                            <div class="six columns picker" style="margin-left: 13px">
                                                <small>ITEM CATEGORY</small>
                                                <select name="ctg">
                                                    <option <?=$p->ItemCtg == 1 ? 'selected' : ''?> value="1">Enhancements</option>
                                                    <option <?=$p->ItemCtg == 2 ? 'selected' : ''?> value="2">EXP Rosaries</option>
                                                    <option <?=$p->ItemCtg == 3 ? 'selected' : ''?> value="3">Destiny Boxes</option>
                                                    <option <?=$p->ItemCtg == 4 ? 'selected' : ''?> value="4">Pet System</option>
                                                    <option <?=$p->ItemCtg == 5 ? 'selected' : ''?> value="5">Accessories</option>
                                                    <option <?=$p->ItemCtg == 6 ? 'selected' : ''?> value="6">Miscellaneous</option>
                                                    <option <?=$p->ItemCtg == 7 ? 'selected' : ''?> value="7">Weapons</option>
                                                    <option <?=$p->ItemCtg == 8 ? 'selected' : ''?> value="8">Costumes</option>
                                                    <option <?=$p->ItemCtg == 9 ? 'selected' : ''?> value="9">Potions</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="six columns">
                                                <small>ITEM SCREENSHOT</small>
                                                <input type="file" class="input" name="ss" />
                                            </div>
                                            <div class="six columns picker">
                                                <small>IS ITEM UNLI?</small>
                                                <select name="isunli">
                                                    <option <?=$p->IsUnli == 0 ? 'selected' : ''?> value="0">No</option>
                                                    <option <?=$p->IsUnli == 1 ? 'selected' : ''?> value="1">Yes</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="six columns picker">
                                                <small>Hidden?</small>
                                                <select name="hid">
                                                    <option <?=$p->hidden == 0 ? 'selected' : ''?> value="0">No</option>
                                                    <option <?=$p->hidden == 1 ? 'selected' : ''?> value="1">Yes</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <small>ITEM DESCRIPTION</small>
                                            <textarea name="comment" class="input textarea" placeholder="Comment"><?=$p->ItemComment?></textarea>
                                        </div>

                                        <center>
                                            <input type="submit" value="Update Item" class="medium success btn">
                                        </center>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>


    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" media="screen" title="no title" charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="<?=base_url()?>res/js/cs.common.js" charset="utf-8"></script>
    <script src="<?=base_url()?>res/js/libs/gumby.min.js" charset="utf-8"></script>

</html>
