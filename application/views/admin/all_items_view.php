<?php $this->load->view('admin/inc/head_view') ?>
<?php $this->load->view('admin/inc/topbar_view') ?>
<?php $this->load->view('admin/inc/nav_view') ?>


        <div class="nine columns">
            <div class="row main-content">
                <div class="twelve columns">
                    <div class="panel">
                        <div class="panel-header">
                            <h3 class="panel-title">Item Shop List</h3>
                            <p class="panel-sub">List of all Items in Item Shop.</p>
                        </div>

                        <div class="panel-content">
                            <?php if ($this->session->flashdata('err') !== null): ?>
                                <div class="<?=$this->session->flashdata('err')['status'] == 0 ? 'danger' : 'success'?> alert" style="font-size: 10px !important;">
                                    <?=$this->session->flashdata('err')['error']?>
                                </div>
                            <?php endif; ?>
                            <table id="topup">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Sec</th>
                                        <th>Stock</th>
                                        <th>Price</th>
                                        <th>Is Unli</th>
                                        <th>Tools</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Title</th>
                                        <th>Sec</th>
                                        <th>Stock</th>
                                        <th>Price</th>
                                        <th>Is Unli</th>
                                        <th>Tools</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php foreach ($this->Shop_Model->get_all() as $key => $var): ?>
                                        <tr>
                                            <td><?=$var->ItemName?></td>
                                            <td><?=$var->ItemSec == 1 ? 'EP' : 'VP'?></td>
                                            <td><?=$var->Itemstock?></td>
                                            <td><?=$var->ItemPrice?></td>
                                            <td><?=$var->IsUnli == 1 ? 'Yes' : 'No'?></td>
                                            <td>
                                                <a href="<?=base_url()?>admin/item/edit/<?=$var->ProductNum?>/"><button class="info label">Edit</button></a>
                                                <a href="<?=base_url()?>admin/item/delete/<?=$var->ProductNum?>/"><button class="danger label" onclick="return confirm('Are you sure?')">Delete</button></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>


    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" media="screen" title="no title" charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="<?=base_url()?>res/js/cs.common.js" charset="utf-8"></script>
    <script src="<?=base_url()?>res/js/libs/gumby.min.js" charset="utf-8"></script>
    <script src="http://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" charset="utf-8"></script>
    <script type="text/javascript">
       $(document).ready(function(){
           $('#topup').DataTable();
       });
   </script>

</html>
