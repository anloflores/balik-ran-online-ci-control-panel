<?php $this->load->view('admin/inc/head_view') ?>
<?php $this->load->view('admin/inc/topbar_view') ?>
<?php $this->load->view('admin/inc/nav_view') ?>


        <div class="nine columns">
            <div class="row main-content">
                <div class="twelve columns">
                    <div class="panel">
                        <div class="panel-header">
                            <h3 class="panel-title">Character Infomartion of <?=$c->ChaName?><h3>
                            <p class="panel-sub">You can edit the informatin of the current character.</p>
                        </div>

                        <div class="panel-content">
                            <div class="row">
                                <div class="centered eight columns">
                                    <?php if ($this->session->flashdata('err') !== null): ?>
                                        <div class="<?=$this->session->flashdata('err')['status'] == 0 ? 'danger' : 'success'?> alert" style="font-size: 10px !important;">
                                            <?=$this->session->flashdata('err')['error']?>
                                        </div>
                                    <?php endif; ?>

                                    <form class="field" action="<?=base_url()?>admin/points/info/<?=$user?>/<?=$c->ChaNum?>/" method="post" enctype="multipart/form-data">
                                        <div class="row">
                                            <small>IN GAME NAME</small>
                                            <input type="text" class="input" name="name" value="<?=$c->ChaName?>" placeholder="Name">
                                        </div>

                                        <div class="row">
                                            <div class="six columns">
                                                <small>School</small><br/>
                                                <div class="twelve columns picker">
                                                    <select name="school" style="width: 100%;">
                                                        <option <?=$c->ChaSchool == 0 ? 'selected' : ''?> value="0">SG</option>
                                                        <option <?=$c->ChaSchool == 1 ? 'selected' : ''?> value="1">MP</option>
                                                        <option <?=$c->ChaSchool == 2 ? 'selected' : ''?> value="2">PHX</option>
                                                        <option <?=$c->ChaSchool == 4 ? 'selected' : ''?> value="4">None</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <small>STATS</small>
                                        <div class="row">
                                            <div class="four columns">
                                                <small>Strong</small>
                                                <input type="text" class="input" name="stro" value="<?=$c->ChaStrong?>" placeholder="Strong">
                                            </div>
                                            <div class="four columns">
                                                <small>Strenght</small>
                                                <input type="text" class="input" name="str" value="<?=$c->ChaStrength?>" placeholder="STR">
                                            </div>
                                            <div class="four columns">
                                                <small>Dex</small>
                                                <input type="text" class="input" name="dex" value="<?=$c->ChaDex?>" placeholder="DEX">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="four columns">
                                                <small>Spirit</small>
                                                <input type="text" class="input" name="spi" value="<?=$c->ChaSpirit?>" placeholder="SPI">
                                            </div>

                                            <div class="four columns">
                                                <small>Power</small>
                                                <input type="text" class="input" name="pow" value="<?=$c->ChaPower?>" placeholder="POW">
                                            </div>

                                            <div class="four columns">
                                                <small>Inteligence</small>
                                                <input type="text" class="input" name="int" value="<?=$c->ChaIntel?>" placeholder="INT">
                                            </div>
                                        </div>

                                        <small>POINTS</small>

                                        <div class="row">
                                            <div class="six columns">
                                                <small>E-Points</small>
                                                <input type="text" class="input" name="ep" value="<?=$p->Points?>" placeholder="EP">
                                            </div>
                                            <div class="six columns">
                                                <small>V-Points</small>
                                                <input type="text" class="input" name="vp" value="<?=$p->VPoints?>" placeholder="VP">
                                            </div>
                                        </div>

                                        <center>
                                            <input type="submit" value="Update Character" class="medium success btn">
                                        </center>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>


    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" media="screen" title="no title" charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="<?=base_url()?>res/js/cs.common.js" charset="utf-8"></script>
    <script src="<?=base_url()?>res/js/libs/gumby.min.js" charset="utf-8"></script>

</html>
