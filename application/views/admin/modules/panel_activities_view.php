<div class="panel">
    <div class="panel-header">
        <h3 class="panel-title">Users Activities</h3>
        <p class="panel-sub">All Users Web Activities</p>
    </div>

    <div class="panel-content">
        <ul class="user-timeline">
            <?php foreach ($this->CP_Model->get_activities() as $key => $v): ?>
                <?php
                    $u = "";
                    if($v->type == 1) {
                        $u = $this->User_Model->get_usernum($v->userid)->UserName;
                    } else {

                    }
                 ?>
                <li class="timeline-item" data-content="" data-color="blue">
                    <p class="timeline-title"><a href="#"><?=$u?></a></p>
                    <div class="timeline-sub">
                        <small><i class="icon-clock"> </i> <?=$v->date_created?></small>
                    </div>
                    <p style="font-size: 12px;">
                         <?=$v->description?>
                    </p>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>

    <div class="panel-footer">

    </div>
</div>
